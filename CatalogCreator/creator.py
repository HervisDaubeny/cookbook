"""TODO: script description"""
import os
import argparse

""" Constants """
PROJECT_NAME = "SebastiansCookBook"
ROOT_NS = "SCBook"
TEMPLATE_PATH = "..\\CatalogTemplate"
DEPLOY_PATH = "..\\" + PROJECT_NAME + "\\"
TEST_PATH = "..\\TestCatalogBuilderDir"
CONTEXT_PATH = DEPLOY_PATH + "\\" + ROOT_NS + ".Dal\\EventOrgContext.cs"
STARTUP_PATH = DEPLOY_PATH + "\\" + ROOT_NS + ".WebApi\\Startup.cs"
REGISTRATION_PLACEHOLDER = "/* CATALOG REGISTRATION PLACEHOLDER */"
SEED_PLACEHOLDER = "/* CATALOG SEED PLACEHOLDER */"
SERVICE_PLACEHOLDER = "/* SERVICE PLACEHOLDER */"


def get_parser():
    """
    Create parser object and set arguments for new catalog.

    return: created parser
    """
    parser_desc = 'Create new catalog with name specified by argument value.'
    deploy_desc = 'This argument confirms deployment of the new catalog.'
    catalog_desc = 'After this option comes the name for the new catalog.'

    pars = argparse.ArgumentParser(description=parser_desc)
    pars.add_argument('-d', '--deploy', action='store_true', help=deploy_desc)
    pars.add_argument('-c', '--catalog', dest='catalog', type=str, help=catalog_desc)

    return pars


def load_file_list(path):
    """
    Scan the CatalogTemplate directory and create a list of paths to files.

    param path: Path to CatalogTemplate directory
    return: List of paths to all files in the template directory
    """
    files = []
    ext = [".cs", ".txt"]
    for f in os.scandir(path):
        if f.is_dir():
            files += load_file_list(f)
        if f.is_file():
            if os.path.splitext(f.name)[1].lower() in ext:
                files.append(f.path)

    return files


def find_and_replace(old_word, new_word, line):
    """
    Replace all keywords in the given line.

    param old_word: Word to replace
    param new_word: Word to replace with
    param line: Line to replace the words in
    return: New line with the replaced words
    """
    begin = line.find(old_word)
    while begin >= 0:
        line = line[:begin] + new_word + line[begin+len(old_word):]
        begin = line.find(old_word)

    return line


def create_new_file(old_word, new_word, source_file, target_path):
    """
    Create copy of file from template at correct location.
    Replace all keywords.

    param old_word: Word to replace
    param new_world: Word to replace with
    param source_file: File opened for reading
    param target_path: Path to file to open for writing
    """
    os.makedirs(os.path.dirname(target_path), exist_ok=True)
    with open(target_path, 'w') as target_file:
        # don't copy contents of the seed file
        if target_path.find(f"Dal.Seeds.{new_word}Seed.txt") >= 0:
            return

        sf_old_word = old_word[0].lower() + old_word[1:]
        sf_new_word = new_word[0].lower() + new_word[1:]

        for line in source_file:
            new_line = find_and_replace(old_word, new_word, line)
            new_line = find_and_replace(sf_old_word, sf_new_word, new_line)
            target_file.write(new_line)


def count_alignment(line):
    """
    Count preceding whitespaces in the given line.

    param line: Line to use
    return: Number of preceding whitespaces
    """
    counter = 0
    for char in line:
        if char.isspace():
            counter += 1
        else:
            break

    return counter


def register_and_seed(catalog):
    """
    Register catalog as DbSet and for seeding.

    param catalog: Catalog to register
    """
    db_set_registration = f"public DbSet<{catalog}> {catalog}s {{ get; set; }} = default!;\n"
    seed_catalog = f"//TODO: seed the {catalog} catalog\n"

    context = []
    registration_index = -1
    seed_index = -1
    registration_align = 0
    seed_align = 0

    # read the context file and find the registration placeholders
    with open(CONTEXT_PATH, 'r') as file:
        index = 0
        for line in file:
            context.append(line)

            # check the catalog registration placeholder
            if line.find(REGISTRATION_PLACEHOLDER) >= 0:
                registration_index = index
                registration_align = count_alignment(line)

            # check the catalog seed placeholder
            if line.find(SEED_PLACEHOLDER) >= 0:
                seed_index = index + 1
                seed_align = count_alignment(line)
            index += 1

    # register the catalogs
    if registration_index > 0 and seed_index > 0:
        registration_text = registration_align * ' ' + db_set_registration
        seed_text = seed_align * ' ' + seed_catalog
        context.insert(registration_index, registration_text)
        context.insert(seed_index, seed_text)

    # update the context file
    with open(CONTEXT_PATH, 'w') as file:
        file.writelines(context)


def register_service(service):
    """
    Register given Service and its interface to startup.

    param service: Service to register
    """
    registration_align = 0
    registration_index = -1
    registration_text = f"services.AddTransient<I{service}, {service}>();\n"
    startup = []

    # read the startup file and find the service placeholder
    with open(STARTUP_PATH, 'r') as file:
        index = 0
        for line in file:
            startup.append(line)

            # check the placeholder
            if line.find(SERVICE_PLACEHOLDER) >= 0:
                registration_align = count_alignment(line)
                registration_index = index
            index += 1

    # register the service
    if registration_index > 0:
        registration_text = registration_align * ' ' + registration_text
        startup.insert(registration_index, registration_text)

    # update the startup file
    with open(STARTUP_PATH, 'w') as file:
        file.writelines(startup)


if __name__ == "__main__":
    """ main method done the Python way. """
    root_dir = TEST_PATH
    deploy = False
    old_catalog = "Author"
    new_catalog = "Test"

    # check for the arguments
    parser = get_parser()
    args = parser.parse_args()
    if args.catalog:
        new_catalog = args.catalog
    if args.deploy:
        deploy = True
        root_dir = DEPLOY_PATH

    # load paths form the template directory
    paths = load_file_list(TEMPLATE_PATH)
    new_paths = []

    # create new paths
    for p in paths:
        # p[len(template_dir):] skips exactly the length of template dir
        old_path = p[len(TEMPLATE_PATH):]
        new_path = find_and_replace(old_catalog, new_catalog, old_path)
        new_paths.append(f"{root_dir}{new_path}")

    # create the new catalog
    for (source, target) in zip(paths, new_paths):
        with open(source, 'r') as src:
            create_new_file(old_catalog, new_catalog, src, target)

    # in case of deployment register the catalog and its service
    if deploy:
        #register_and_seed(new_catalog)
        #register_service(f"{new_catalog}Service")
        ...
