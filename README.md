# Sebastians Cookbook

The task is to create a simple cookbook website API that will manage recipes divided to categories. The goal is to prepare a backend that will support CRUD operations used by this website.

Link to a reference page: https://www.vareni.cz/recepty/stankove-langose/

## Implemented

Please view [CHANGELOG](CHANGELOG.md) to see implemented features in current release.
