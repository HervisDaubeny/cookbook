# Codereview - Sebastian

## GIT
- Logicke celky squashovat
- Urcite by tam nemely byt zemergovane WIP commity

## Docker
- OK

## SCBook.Core
### DTOs
- OK
### Services
- Chyby dokumentace k jednotlivým metodám
_- Stránkování by mělo být děláno na straně databáze a ne v paměti `.SelectBy()` - logictejsi nazev
- `KeyDoesNotExistException` a `DbSetExtensions` by mel byt asi uz rovnou v DALu
- `UpdateWithJoining` RemoveRange misto foreach Remove + check zda vubec delat upodate, context.SaveChanges by tu nemel byt
### Mappers
- OK


## SCBook.Dal
- K datům přistupovat asynchronně je-li to možné: asynchronní SQL query, které pak vedou na asynchronní Action methods of Controllers (API)
- Relace na další entity by měly být `virtual` (aby EF mohl vytvořil proxy)
- U složitějších projetu použit Repository pattern (další decoupling vrstva) - tady je to OK.
- U definice DB entit okomentovat jednotlivé columns, at je vidět k čemu slouží. 
- Smysl rozdělování entity na `IEntity` vs `IJoiningEntity`
- Do databáze ukládat kompletní čas včetně zóny: `DateTimeOffset`

- Trochu logičtější pojmenování properties
   * `Value` (moc obecný název)
   * `ReviewersName` - spíš bych použil `AuthorName`
   * `TimeOfReview` - mi evokuje dobu trvání review než čas vytvoření review (`ReviewDate`)

## SCBook.Tests
- Pro tento jednoduchy projekt prime volani API, pres TestHost je dostacujici
- Do budoucna se podivat jak psat LightBDD flow testy a vygenrovat test clienta ze Swagger popisu
- `Cleanup` se pak resi v ramci dispouse testovaciho Contextu LightBDD framework

## SCBook.WebApi
### Controllers
- `RecipeFilter` validator jde udělat přes FluentValidator
- Asynchronní API
### Swagger (examples)
- OK
### Messages
- oanotovat s `[JsonPropertyName]`
- `GetRecipeDto`: z performace duvodu u katalogu vetsinou vracet jen IDs (na FE budou mit catalogy jiz nactene v cache). Ulehci to pak i dotazum do SQL
- mnozna cislo pro endpoitn Controlleru
### Fakers
- OK
### Validation
- OK
### Mappers
- mužou byt internal (slouži jen pro vnitřni užití - nikam ven je nabízet nebudeme)
### StartUp
- OK (super rozcleneni logicke celky pres extensions)