# Encountered problems

## Open problems

---

### Mystery of Visual Studio Configurations
Thing I'm not getting here is very general, how do the configurations in the Visual Studio project work?

For each project there exists this `launchSettings.json` file which contains different logs/configurations. Each of these has its own `Profile`, `Launch`
`Environment variables` and other items.

#### 1) Production

During app development it was necessary to create a different connection string since the production version of the app was being deployed to Azure.
I created corresponding connection string in `appsettings.Production.json` and my colleague helped me and set up the environmental variable `ASPNETCORE_ENVIRONMENT = Production` for me.

*this is the configuration from `launchSettings.json` it created*
```json
"SebastiansCookbook": {
      "commandName": "Project",
      "launchBrowser": true,
      "environmentVariables": {
        "ASPNETCORE_ENVIRONMENT": "Production"
      },
      "dotnetRunMessages": "true",
      "applicationUrl": "https://localhost:5001;http://localhost:5000"
    },
```

Why is the `Profile` equal to "SebastiansCookbook" and not "ISS Express" or "Docker"? *- SebastiansCookbook is name the solution, does this relate to more projects (whole solution) being built at once?*

Why is the `Launch` equal to "Project" and not one of {"ISS", "ISS Express", "Snapshot Debugger", "Docker", "Executable"}
 *- in fact, what are those anyways?*

### 2) Testing environment
When creating tests for the application *(in separate project referencing the WebApi project)*  
I had a problem that I believe originated from my lack of understanding of the topic of the first part of this post.

I had to create another connection string for the HttpClient I used for the tests. Following the practice of file per environment, I created `appsettings.Testing.json` file and put the connection string there.

The problem I encountered was "How to force the WebApplicationBuilder to use this appsettings file". And even tho the solution is relatively simple and already presented in the following thread: https://stackoverflow.com/questions/62912223/set-environment-in-integration-tests I still have a question.

What I wanted to do before I was recommended this solution was to create new "configuration" for the WebApi project. I thought that if I specified the environmental variable `ASPNETCORE_ENVIRONMENT = Testing` I will have succeeded. There are couple of points before my question:

1) I haven't thought about how would I make the WebApi project to use this configuration from the Testing project...
2) I couldn't understand how would I even create the configuration - I thought as much as that if I were to create the configuration for the same `Profile` and `Launch` but with a different value to the environmental variable `ASPNETCORE_ENVIRONMENT` there would be a conflict  
So my idea here was to use different `Profile` or at least `Launch` but since I didn't understand their meaning I couldn't. *(Here actually came the question of forcing the project to use a configuration...)*

Now to the question:  Is it possible to set a project to use a configuration from another project? Is it also possible to somehow create another configuration like I wanted to do?

---

### Mystery of Connection strings

The problem I encountered here was with `Host`.

When using `Host=localhost;Port=5432;...` when building the application *over Docker Compose* the connection fails with the following error:

```
Npgsql.NpgsqlException: 'Exception while connecting'
Inner Exception
ExtendedSocketException: Cannot assign requested address [::1]:5432
```

The solution to the problem is to simply use `Host=host.docker.inner;Port=5432;...` But what I do not get is what is actually wrong here. The database indeed runs on `localhost:5432` which I confirmed by connecting to it using `pgAdmin` tool and this exact address as a server URL. So how is it possible to get the error I'm having?

---
---

## Solved problems

---

### DB migration

To do a DB migration from PMC I need to specify the target project & the startup project.
```
add-migration CreateSCBookDB -Project ****.Dal.SQL -StartupProject ****.WebApi
```

```
Update-database CreateSCBookDB -Project ****.Dal.SQL -StartupProject ****.WebApi
```

Unlike when doing it over `.bat` file I need to manualy specify the server and the port in the `ConnectionString` so the Postre will connect correctly.

```json
{
    "ConnectionStrings": {
        "ConnectionString": "Host=host.docker.inner;Port=5432;Database=SCBook.Development;Username=****;Password=****"
    }
}
```

If not specified the update will end with an error:

```
System.Net.Sockets.SocketException (11001): No such host is known
...
```
*And possibly some others...*

---

### Json can be a folder

When adding a `ConnectionString` as an enviromental variable using `appsettings.json` remember there is a `appsettings.Development.json` file where the variable is supposed to live.

---

### Authentication

I was following this tutorial https://www.c-sharpcorner.com/article/authentication-and-authorization-in-asp-net-core-web-api-with-json-web-tokens/

The tutorial is fine, but i managed to install one too many nuget package https://www.nuget.org/packages/Microsoft.AspNetCore.Identity.UI which then broke my applications ability to do migrations.

---

### LDBB tests not running

It is necessary to have https://www.nuget.org/packages/NUnit3TestAdapter/ nuget package when implementing tests with LDBB.Nunit.

Also when using LDBB.Xunit or LDBB.Nunit it is necessary to specify the testing assembly.

```cs
[assembly: LightBddScope]
// here goes the namespace {
//  ...
// }
```

---
