# Changelog

---
---
## release v1.1.0
---
*This release focuses on applying agreed upon changes suggested in* [codereview v1.0.1](codereview_v101.md)

### Added
#### LBDD test

Implemented test using `LBDD.NUnit` framework.

#### Filtration&Pagination
Created message and DTO for Filtration and Validation. Set up fluent validation of the message.

---
### Updated
#### Tests
Created connection string `appsettings.Testing.json` and HttpClient now runs in `Testing` environment.

#### Datetime
Started using `DateTimeOffset` instead of `DateTime`.

#### Recipe Update
Fixed the suboptimal updating of the `Recipe` entity's N:M relations. Now `.Skip()` and `.Take()` LINQ methods are used directly on the query.

#### Filtration&Pagination
Aside from reimplementing Filtration and Pagination to use new message and DTO, more common method of pagination was used. One working with `offset` and `take` instead of `page_size` and `page_number`.

---
### Removed
#### IJoiningEntity
Removed unused interface.

#### RecipePaginationExtensions
Removed `RecipePaginationExtensions` and expanded the Pagination directly to the `RecipeService`.

---
---
## hotfix v1.0.1
---
### Updated
#### Fixed authentication

In release v1.0.0 I forgot to add Authentication setting to the `appsettings.Production.json`. This hotfix fixes that problem.

---
---
## release v1.0.0
---
### Added
#### API implementation

Catalogs:
- Author
- Difficulty
- Category
- Ingredient
- Special Tool

Entities:
- Recipe
- Review

#### Features

Filtration:
- Difficulty
- Time to prepare
- Ingredient
- Title

Pagination:
- 10 - 100 sized pages

Authentication:
- CDU on `Recipe` is allowed only with Administrator premissions

Tests:
- Simple test as POC
- Business Scenario integration test
    - Creates resources
    - Creates Reviews, Concurently likes
    - Verifies integrity
    - Deletes resources  
    **resources as db resources, nothing to do with Azure*

#### Azure

Pipelines:
- build pipeline triggered on `develop` branch
- deploy pipeline triggered on `master` branch

Infrastructure:
- Linux App Service Plan
- Azure Web App (for ASP.Net Core app)
- Postgre Db

---
