﻿using System;

namespace SCBook.Dal.Entities
{
    public class Review : IEntity
    {
        public Guid Id { get; set; }
        public string Value { get; set; } = default!;
        public string ReviewersName { get; set; } = default!;
        public DateTimeOffset TimeOfReview { get; set; }
        public bool PublishNameConsent { get; set; } = default!;

        public Guid RecipeId { get; set; }
        public Recipe Recipe { get; set; } = default!;
    }
}
