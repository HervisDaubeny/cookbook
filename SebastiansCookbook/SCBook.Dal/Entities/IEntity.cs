﻿using System;

namespace SCBook.Dal.Entities
{
    public interface IEntity
    {
        public Guid Id { get; set; }
    }
}
