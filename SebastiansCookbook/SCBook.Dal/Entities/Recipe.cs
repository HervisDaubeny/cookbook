﻿using SCBook.Dal.Catalogs;
using SCBook.Dal.JoiningEntities;
using System;
using System.Collections.Generic;

namespace SCBook.Dal.Entities
{
    public class Recipe : IEntity
    {
        public Guid Id { get; set; }
        public string Title { get; set; } = default;
        public int PrepTime { get; set; }
        public string Directions { get; set; } = default;
        public int LikeCount { get; set; }
        public string PhotoLink { get; set; }

        public Guid AuthorId { get; set; }
        public Author Author { get; set; }

        public Guid DifficultyId { get; set; }
        public Difficulty Difficulty { get; set; }

        public virtual ICollection<RecipeCategory> RecipeCategories { get; set; } = new HashSet<RecipeCategory>();

        public virtual ICollection<RecipeIngredient> RecipeIngredients { get; set; } = new HashSet<RecipeIngredient>();

        public virtual ICollection<RecipeSpecialTool> RecipeSpecialTools { get; set; } = new HashSet<RecipeSpecialTool>();

        public virtual ICollection<Review> Reviews { get; set; } = new HashSet<Review>();
    }
}
