﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SCBook.Dal.Catalogs;
using SCBook.Dal.Entities;
using SCBook.Dal.JoiningEntities;

namespace SCBook.Dal
{
    public class SCBookContext : IdentityDbContext<VerifiedUser>
    {
        public SCBookContext(DbContextOptions<SCBookContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(this.GetType().Assembly);
        }

        // Entities
        public DbSet<Recipe> Recipes { get; set; } = default;
        public DbSet<Review> Reviews { get; set; } = default!;

        // Catalogs
        public DbSet<Author> Authors { get; set; } = default!;
        public DbSet<Category> Categories { get; set; } = default!;
        public DbSet<Difficulty> Difficulties { get; set; } = default!;
        public DbSet<Ingredient> Ingredients { get; set; } = default!;
        public DbSet<SpecialTool> SpecialTools { get; set; } = default!;

        // Joining Entities
        public DbSet<RecipeCategory> RecipeCategories { get; set; } = default!;
        public DbSet<RecipeIngredient> RecipeIngredients { get; set; } = default!;
        public DbSet<RecipeSpecialTool> RecipeSpecialTools { get; set; } = default!;
    }
}
