﻿using SCBook.Dal.Catalogs;
using SCBook.Dal.Entities;
using System;

namespace SCBook.Dal.JoiningEntities
{
    public class RecipeSpecialTool
    {
        public Guid RecipeId { get; set; }
        public Recipe Recipe { get; set; }

        public Guid SpecialToolId { get; set; }
        public SpecialTool SpecialTool { get; set; }
    }
}
