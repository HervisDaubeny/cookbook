﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCBook.Dal.JoiningEntities;

namespace SCBook.Dal.Mappings.JoiningEntities
{
    class RecipeSpecialToolMapping : IEntityTypeConfiguration<RecipeSpecialTool>
    {
        public void Configure(EntityTypeBuilder<RecipeSpecialTool> builder)
        {
            builder
                .HasKey(rc => new { rc.RecipeId, rc.SpecialToolId });

            builder
                .HasOne(rc => rc.Recipe)
                .WithMany(r => r.RecipeSpecialTools)
                .HasForeignKey(rc => rc.RecipeId);

            builder
                .HasOne(rc => rc.SpecialTool)
                .WithMany(s => s.RecipeSpecialTools)
                .HasForeignKey(rc => rc.SpecialToolId);
        }
    }
}
