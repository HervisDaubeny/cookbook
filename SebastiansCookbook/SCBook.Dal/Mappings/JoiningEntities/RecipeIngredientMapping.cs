﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCBook.Dal.JoiningEntities;

namespace SCBook.Dal.Mappings.Catalogs.JoiningEntities
{
    class RecipeIngredientMapping : IEntityTypeConfiguration<RecipeIngredient>
    {
        public void Configure(EntityTypeBuilder<RecipeIngredient> builder)
        {
            builder
                .HasKey(rc => new { rc.RecipeId, rc.IngredientId });

            builder
                .HasOne(rc => rc.Recipe)
                .WithMany(r => r.RecipeIngredients)
                .HasForeignKey(rc => rc.RecipeId);

            builder
                .HasOne(rc => rc.Ingredient)
                .WithMany(i => i.RecipeIngredients)
                .HasForeignKey(rc => rc.IngredientId);
        }
    }
}
