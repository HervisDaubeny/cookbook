﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCBook.Dal.Entities;

namespace SCBook.Dal.Mappings.Entities
{
    public class ReviewMapping : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            // PK
            builder
                .HasKey(_ => _.Id);

            // Value
            builder
                .Property(_ => _.Value)
                .HasMaxLength(500)
                .IsRequired();

            // ReviewersName
            builder
                .Property(_ => _.ReviewersName)
                .HasMaxLength(150)
                .IsRequired();

            // TimeOfReview
            builder
                .Property(_ => _.TimeOfReview)
                .IsRequired();

            // PublishNameConsent
            builder
                .Property(_ => _.PublishNameConsent)
                .IsRequired();
        }
    }
}
