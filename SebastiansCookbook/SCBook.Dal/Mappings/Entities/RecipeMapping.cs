﻿using SCBook.Dal.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SCBook.Dal.Mappings
{
    class RecipeMapping : IEntityTypeConfiguration<Recipe>
    {
        public void Configure(EntityTypeBuilder<Recipe> builder)
        {
            // PK
            builder
                .HasKey(_ => _.Id);

            // Title
            builder
                .Property(_ => _.Title)
                .HasMaxLength(300)
                .IsRequired();

            // PrepTime
            builder
                .Property(_ => _.PrepTime)
                .IsRequired();

            // Directions
            builder
                .Property(_ => _.Directions)
                .HasMaxLength(2000)
                .IsRequired();

            // LikeCount
            builder
                .Property(_ => _.LikeCount)
                .IsRequired();

            // 1:N relation to Author
            builder
                .HasOne(r => r.Author)
                .WithMany(a => a.Recipes)
                .HasForeignKey(r => r.AuthorId);

            // 1:N relation to Difficulty
            builder
                .HasOne(r => r.Difficulty)
                .WithMany(d => d.Recipes)
                .HasForeignKey(r => r.DifficultyId);

            // N:1 relation to Review
            builder
                .HasMany(rec => rec.Reviews)
                .WithOne(rev => rev.Recipe)
                .HasForeignKey(rev => rev.RecipeId);
        }
    }
}
