﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCBook.Dal.Catalogs;

namespace SCBook.Dal.Mappings.Catalogs
{
    public class IngredientMapping : IEntityTypeConfiguration<Ingredient>
    {
        public void Configure(EntityTypeBuilder<Ingredient> builder)
        {
            // PK
            builder
                .HasKey(_ => _.Id);

            // Value
            builder
                .Property(_ => _.Value)
                .HasMaxLength(150)
                .IsRequired();

            // Active
            builder
                .Property(_ => _.Active)
                .IsRequired();
        }
    }
}
