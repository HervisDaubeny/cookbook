﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SCBook.Dal.Catalogs;

namespace SCBook.Dal.Mappings.Catalogs
{
    public class AuthorMapping : IEntityTypeConfiguration<Author>
    {
        public void Configure(EntityTypeBuilder<Author> builder)
        {
            // PK
            builder
                .HasKey(_ => _.Id);

            // Value
            builder
                .Property(_ => _.Value)
                .HasMaxLength(150)
                .IsRequired();

            // Active
            builder
                .Property(_ => _.Active)
                .IsRequired();
        }
    }
}
