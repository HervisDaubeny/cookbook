﻿using SCBook.Dal.Entities;
using System;
using System.Collections.Generic;

namespace SCBook.Dal.Catalogs
{
    public class Author : ICatalogEntity
    {
        public Guid Id { get; set; }
        public string Value { get; set; } = default!;
        public bool Active { get; set; } = default!;

        public virtual ICollection<Recipe> Recipes { get; set; } = new HashSet<Recipe>();
    }
}
