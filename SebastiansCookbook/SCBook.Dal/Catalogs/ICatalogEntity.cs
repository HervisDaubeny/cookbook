﻿using System;

namespace SCBook.Dal.Catalogs
{
    public interface ICatalogEntity
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public bool Active { get; set; }
    }
}
