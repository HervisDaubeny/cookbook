﻿using SCBook.Dal.JoiningEntities;
using System;
using System.Collections.Generic;

namespace SCBook.Dal.Catalogs
{
    public class Ingredient : ICatalogEntity
    {
        public Guid Id { get; set; }
        public string Value { get; set; } = default!;
        public bool Active { get; set; } = default!;

        public virtual ICollection<RecipeIngredient> RecipeIngredients { get; set; } = new HashSet<RecipeIngredient>();
    }
}
