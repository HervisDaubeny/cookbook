﻿using SCBook.Dal.JoiningEntities;
using System;
using System.Collections.Generic;

namespace SCBook.Dal.Catalogs
{
    public class SpecialTool : ICatalogEntity
    {
        public Guid Id { get; set; }
        public string Value { get; set; } = default!;
        public bool Active { get; set; } = default!;

        public virtual ICollection<RecipeSpecialTool> RecipeSpecialTools { get; set; } = new HashSet<RecipeSpecialTool>();
    }
}
