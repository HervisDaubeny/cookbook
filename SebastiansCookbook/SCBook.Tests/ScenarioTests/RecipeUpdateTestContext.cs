﻿using LightBDD.NUnit3;
using NUnit.Framework;
using SCBook.WebApi.Authentication;
using SCBook.WebApi.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

[assembly: LightBddScope]
namespace SCBook.Tests.LBDD
{
    public partial class RecipeUpdateTests : ScenarioTestBaseClass
    {
        private string _authToken;
        private LoginModel _credentials;
        private HttpClient _verifiedClient;
        private CreateAuthorRequest _createAuthor;
        private CreateDifficultyRequest _createDifficulty;
        private CreateRecipeRequest _createRecipeRequest;
        private CreateCategoryRequest[] _createCategories;
        private CreateIngredientRequest[] _createIngredients;
        private CreateSpecialToolRequest[] _createSpecialTools;
        private UpdateRecipeRequest _updateRecipeRequest;
        private Guid _authorId;
        private Guid _difficultyId;
        private Guid _recipeId;
        private Guid[] _categoryIds;
        private Guid[] _ingredientIds;
        private Guid[] _specialToolIds;
        private HttpResponseMessage _updateRecipeResponse;
        private HttpResponseMessage[] _cleanupResponses;

        private void Given_valid_credentials(string username, string password)
        {
            _credentials = new LoginModel() { Username = username, Password = password };
        }

        private void Admin_should_log_in_successfuly()
        {
            var response = TestClient.PostAsJsonAsync("/api/Authenticate/login", _credentials);
            _authToken = response.Result.Content.ReadAsAsync<LoginResponse>().Result.Token;

            Assert.That(response.Result.StatusCode is HttpStatusCode.OK);
        }

        private void Given_catalog_values(string author, string difficulty, string[] categories, string[] ingredients, string[] specialTools)
        {
            _createAuthor = new CreateAuthorRequest { Value = author };
            _createDifficulty = new CreateDifficultyRequest { Value = difficulty };
            _createCategories = categories.Select(c => new CreateCategoryRequest { Value = c }).ToArray();
            _createIngredients = ingredients.Select(i => new CreateIngredientRequest { Value = i }).ToArray();
            _createSpecialTools = specialTools.Select(st => new CreateSpecialToolRequest { Value = st }).ToArray();
        }

        private void Creating_catalogs_should_be_successful()
        {
            var authorResponse = TestClient.PostAsJsonAsync("/api/Author", _createAuthor).Result;
            Assert.That(authorResponse.StatusCode is HttpStatusCode.Created);

            var difficultyResponse = TestClient.PostAsJsonAsync("/api/Difficulty", _createDifficulty).Result;
            Assert.That(difficultyResponse.StatusCode is HttpStatusCode.Created);

            var categoryResponses = _createCategories.Select(cr => TestClient.PostAsJsonAsync("/api/Category", cr).Result).ToList();
            categoryResponses.ForEach(cr => Assert.That(cr.StatusCode is HttpStatusCode.Created));

            var ingredientResponses = _createIngredients.Select(cr => TestClient.PostAsJsonAsync("/api/Ingredient", cr).Result).ToList();
            ingredientResponses.ForEach(ir => Assert.That(ir.StatusCode is HttpStatusCode.Created));

            var specialToolResponses = _createSpecialTools.Select(cr => TestClient.PostAsJsonAsync("/api/SpecialTool", cr).Result).ToList();
            specialToolResponses.ForEach(str => Assert.That(str.StatusCode is HttpStatusCode.Created));

            _authorId = authorResponse.Content.ReadAsAsync<GeneralCreatedIdResponse>().Result.Id;
            _difficultyId = difficultyResponse.Content.ReadAsAsync<GeneralCreatedIdResponse>().Result.Id;
            _categoryIds = categoryResponses.Select(cr => cr.Content.ReadAsAsync<GeneralCreatedIdResponse>().Result.Id).ToArray();
            _ingredientIds = ingredientResponses.Select(ir => ir.Content.ReadAsAsync<GeneralCreatedIdResponse>().Result.Id).ToArray();
            _specialToolIds = specialToolResponses.Select(str => str.Content.ReadAsAsync<GeneralCreatedIdResponse>().Result.Id).ToArray();            
        }

        private void Using_catalog_ids_to_create_recipe_creating_request()
        {
            _createRecipeRequest = new CreateRecipeRequest()
            {
                Title = "My Little Recipe",
                AuthorId = _authorId,
                DifficultyId = _difficultyId,
                PrepTime = 42,
                Directions = "Lorem ipsum dolor sit amet...",
                PhotoLink = "https://someurl.com/image.jpg",
                CategoryIds = new Guid[] { _categoryIds[0], _categoryIds[1] },
                IngredientIds = new Guid[] { _ingredientIds[0], _ingredientIds[1] },
                SpecialToolIds = new Guid[] { _specialToolIds[0], _specialToolIds[1] }
            };
        }

        private void Using_auth_token_to_add_authentication_header_to_verify_http_client()
        {
            _verifiedClient = TestClient;
            _verifiedClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _authToken);
        }

        private void Recipe_should_be_successfuly_created()
        {
            var response = _verifiedClient.PostAsJsonAsync("/api/Recipe", _createRecipeRequest).Result;
            _recipeId = response.Content.ReadAsAsync<GeneralCreatedIdResponse>().Result.Id;
            Assert.That(response.StatusCode is HttpStatusCode.Created);
        }

        private void Recipe_should_have_correct_relations()
        {
            var response = TestClient.GetAsync($"/api/Recipe/{_recipeId}").Result;
            var recipe = response.Content.ReadAsAsync<GetRecipeResponseItem>().Result;

            // Category relations
            Assert.IsTrue(recipe.Categories
                .Select(c => c.Id)
                .Contains(_categoryIds[0]));
            Assert.IsTrue(recipe.Categories
                .Select(c => c.Id)
                .Contains(_categoryIds[1]));

            // Ingredient relations
            Assert.IsTrue(recipe.Ingredients
                .Select(i => i.Id)
                .Contains(_ingredientIds[0]));
            Assert.IsTrue(recipe.Ingredients
                .Select(i => i.Id)
                .Contains(_ingredientIds[1]));

            // SpecialTool relations
            Assert.IsTrue(recipe.SpecialTools
                .Select(st => st.Id)
                .Contains(_specialToolIds[0]));
            Assert.IsTrue(recipe.SpecialTools
                .Select(st => st.Id)
                .Contains(_specialToolIds[1]));
        }

        private void Using_catalog_ids_to_create_recipe_updating_request()
        {
            _updateRecipeRequest = new UpdateRecipeRequest
            {
                Title = "Updated recipe",
                AuthorId = _authorId,
                DifficultyId = _difficultyId,
                Directions = "Updated directions",
                PrepTime = 47,
                PhotoLink = "updated.photo.link.org/42.jpg",
                CategoryIds = new Guid[] { _categoryIds[2], _categoryIds[3] },
                IngredientIds = new Guid[] { _ingredientIds[2], _ingredientIds[3] },
                SpecialToolIds = new Guid[] { _specialToolIds[1], _specialToolIds[2] }
            };
        }

        private void Update_recipe_relations()
        {
            _updateRecipeResponse = _verifiedClient.PutAsJsonAsync($"/api/Recipe/{_recipeId}", _updateRecipeRequest).Result;
        }

        private void Update_should_be_successful()
        {
            Assert.That(_updateRecipeResponse.StatusCode is HttpStatusCode.OK);
        }

        private void Updated_recipe_should_have_correct_relations()
        {
            var response = TestClient.GetAsync($"/api/Recipe/{_recipeId}").Result;
            var recipe = response.Content.ReadAsAsync<GetRecipeResponseItem>().Result;

            // Category relations
            Assert.IsFalse(recipe.Categories
                .Select(c => c.Id)
                .Contains(_categoryIds[0]));
            Assert.IsFalse(recipe.Categories
                .Select(c => c.Id)
                .Contains(_categoryIds[1]));
            Assert.IsTrue(recipe.Categories
                .Select(c => c.Id)
                .Contains(_categoryIds[2]));
            Assert.IsTrue(recipe.Categories
                .Select(c => c.Id)
                .Contains(_categoryIds[3]));

            // Ingredient relations
            Assert.IsFalse(recipe.Ingredients
                .Select(i => i.Id)
                .Contains(_ingredientIds[0]));
            Assert.IsFalse(recipe.Ingredients
                .Select(i => i.Id)
                .Contains(_ingredientIds[1]));
            Assert.IsTrue(recipe.Ingredients
                .Select(i => i.Id)
                .Contains(_ingredientIds[2]));
            Assert.IsTrue(recipe.Ingredients
                .Select(i => i.Id)
                .Contains(_ingredientIds[3]));

            // SpecialTool relations
            Assert.IsFalse(recipe.SpecialTools
                .Select(st => st.Id)
                .Contains(_specialToolIds[0]));
            Assert.IsTrue(recipe.SpecialTools
                .Select(st => st.Id)
                .Contains(_specialToolIds[1]));
            Assert.IsTrue(recipe.SpecialTools
                .Select(st => st.Id)
                .Contains(_specialToolIds[2]));
        }

        private void Cleanup()
        {
            var cleanupResponses = new List<HttpResponseMessage>() { };

            cleanupResponses.Add(_verifiedClient.DeleteAsync($"/api/Recipe/{_recipeId}").Result);
            cleanupResponses.Add(TestClient.DeleteAsync($"/api/Author/{_authorId}").Result);
            cleanupResponses.Add(TestClient.DeleteAsync($"/api/Difficulty/{_difficultyId}").Result);
            cleanupResponses.AddRange(_categoryIds.Select(ci => TestClient.DeleteAsync($"/api/Category/{ci}").Result).ToList());
            cleanupResponses.AddRange(_ingredientIds.Select(ii => TestClient.DeleteAsync($"/api/Ingredient/{ii}").Result).ToList());
            cleanupResponses.AddRange(_specialToolIds.Select(sti => TestClient.DeleteAsync($"/api/SpecialTool/{sti}").Result).ToList());

            _cleanupResponses = cleanupResponses.ToArray();
        }

        private void Cleanup_should_be_completed_successfuly()
        {
            foreach (var response in _cleanupResponses)
            {
                Assert.That(response.StatusCode is HttpStatusCode.NoContent);
            }
        }
    }
}
