﻿using LightBDD.Framework;
using LightBDD.Framework.Scenarios;
using LightBDD.NUnit3;
using NUnit.Framework;

namespace SCBook.Tests.LBDD
{
    [TestFixture]
    [FeatureDescription(
        @"I want to test N:M relation updates
        of Recipe entity.")]
    [Label("Correct-Auth-Token")]
    public partial class RecipeUpdateTests
    {
        [Scenario]
        [Label("Test-1")]
        public void Recipe_Update_With_Auth_Token()
        {
            Runner.RunScenario(
                _ => Given_valid_credentials("Hervis", "Password#123"),
                _ => Admin_should_log_in_successfuly(),
                _ => Given_catalog_values(
                    "Hervis Daubeny",
                    "5",
                    new string[] { "Chinese", "Spicy", "Vietnamese", "Hot" },
                    new string[] { "Pork", "Garlic", "Chicken", "Onion" },
                    new string[] { "Spatula", "Wok pan", "Normal pan" }
                 ),
                _ => Creating_catalogs_should_be_successful(),
                _ => Using_catalog_ids_to_create_recipe_creating_request(),
                _ => Using_auth_token_to_add_authentication_header_to_verify_http_client(),
                _ => Recipe_should_be_successfuly_created(),
                _ => Recipe_should_have_correct_relations(),
                _ => Using_catalog_ids_to_create_recipe_updating_request(),
                _ => Update_recipe_relations(),
                _ => Update_should_be_successful(),
                _ => Updated_recipe_should_have_correct_relations(),
                _ => Cleanup(),
                _ => Cleanup_should_be_completed_successfuly());
        }
    }
}
