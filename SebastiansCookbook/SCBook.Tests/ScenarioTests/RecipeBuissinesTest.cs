﻿using SCBook.WebApi.Messages;
using SCBook.WebApi.Authentication;
using FluentAssertions;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using System.Net.Http.Headers;

namespace SCBook.Tests.BusinessScenario
{
    internal static class GuidContainer
    {
        internal static Guid AuthorId { get; set; }
        internal static Guid DifficultyId { get; set; }
        internal static Guid CategoryChineseId { get; set; }
        internal static Guid CategoryMeatId { get; set; }
        internal static Guid ToolSpatulaId { get; set; }
        internal static Guid ToolWokId { get; set; }
        internal static Guid IngredientPorkId { get; set; }
        internal static Guid IngredientGarlicId { get; set; }
        internal static Guid IngredientOnionId { get; set; }
        internal static Guid IngredientSoySauceId { get; set; }
        internal static Guid RecipeId { get; set; }
        internal static Guid ReviewNo1Id { get; set; }
        internal static Guid ReviewNo2Id { get; set; }
    }
    internal static class GeneralContainer
    {
        internal static int LikeCount;
        internal static string JWT;
    }

    [Collection("Business Scenario")]
    [CollectionDefinition("Business Scenario", DisableParallelization = true)]
    public class Seq_00_Get_JWT : IntegrationTestBaseClass
    {
        [Fact]
        public async Task Get_Auth_Token()
        {
            // Setup
            var credentials = new LoginModel() { Username = "Hervis", Password = "Password#123" };

            // Do
            var response = await TestClient.PostAsJsonAsync("/api/Authenticate/login", credentials);
            GeneralContainer.JWT = (await response.Content.ReadAsAsync<LoginResponse>()).Token;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }
    }

    [Collection("Business Scenario")]
    [CollectionDefinition("Business Scenario", DisableParallelization = true)]
    public class Seq_01_Create_Data : IntegrationTestBaseClass
    {
        [Fact]
        public async Task CreateNew_Author()
        {
            // Setup
            var message = new CreateAuthorRequest() { Value = "Tester No0" };

            // Do
            var response = await TestClient.PostAsJsonAsync("/api/Author", message);
            GuidContainer.AuthorId = (await response.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact]
        public async Task CreateNew_Difficulty()
        {
            // Setup
            var message = new CreateDifficultyRequest() { Value = "1" };

            // Do
            var response = await TestClient.PostAsJsonAsync("/api/Difficulty", message);
            GuidContainer.DifficultyId = (await response.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact]
        public async Task CreateNew_Categories()
        {
            // Setup
            var messageChinese = new CreateDifficultyRequest() { Value = "Chinese" };
            var messageMeat = new CreateDifficultyRequest() { Value = "Meat" };

            // Do
            var responseChinese = await TestClient.PostAsJsonAsync("/api/Category", messageChinese);
            var responseMeat = await TestClient.PostAsJsonAsync("/api/Category", messageMeat);

            GuidContainer.CategoryChineseId = (await responseChinese.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;
            GuidContainer.CategoryMeatId = (await responseMeat.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;

            // Assert
            responseChinese.StatusCode.Should().Be(HttpStatusCode.Created);
            responseMeat.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact]
        public async Task CreateNew_SpecialTools()
        {
            // Setup
            var messageSpatula = new CreateDifficultyRequest() { Value = "Spatula" };
            var messageWok = new CreateDifficultyRequest() { Value = "Wok" };

            // Do
            var responseSpatula = await TestClient.PostAsJsonAsync("/api/SpecialTool", messageSpatula);
            var responseWok = await TestClient.PostAsJsonAsync("/api/SpecialTool", messageWok);

            GuidContainer.ToolSpatulaId = (await responseSpatula.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;
            GuidContainer.ToolWokId = (await responseWok.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;

            // Assert
            responseSpatula.StatusCode.Should().Be(HttpStatusCode.Created);
            responseWok.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact]
        public async Task CreateNew_Ingredients()
        {
            // Setup
            var messagePork = new CreateDifficultyRequest() { Value = "Pork" };
            var messageGarlic = new CreateDifficultyRequest() { Value = "Garlic" };
            var messageOnion = new CreateDifficultyRequest() { Value = "Onion" };
            var messageSoySauce = new CreateDifficultyRequest() { Value = "Soy Sauce" };

            // Do
            var responsePork = await TestClient.PostAsJsonAsync("/api/Ingredient", messagePork);
            var responseGarlic = await TestClient.PostAsJsonAsync("/api/Ingredient", messageGarlic);
            var responseOnion = await TestClient.PostAsJsonAsync("/api/Ingredient", messageOnion);
            var responseSoySauce = await TestClient.PostAsJsonAsync("/api/Ingredient", messageSoySauce);

            GuidContainer.IngredientPorkId = (await responsePork.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;
            GuidContainer.IngredientGarlicId = (await responseGarlic.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;
            GuidContainer.IngredientOnionId = (await responseOnion.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;
            GuidContainer.IngredientSoySauceId = (await responseSoySauce.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;

            // Assert
            responsePork.StatusCode.Should().Be(HttpStatusCode.Created);
            responseGarlic.StatusCode.Should().Be(HttpStatusCode.Created);
            responseOnion.StatusCode.Should().Be(HttpStatusCode.Created);
            responseSoySauce.StatusCode.Should().Be(HttpStatusCode.Created);
        }
    }

    [Collection("Business Scenario")]
    [CollectionDefinition("Business Scenario", DisableParallelization = true)]
    public class Seq_02_Create_Recipe : IntegrationTestBaseClass
    {
        [Fact]
        public async Task CreateNew_Recipe_Without_JWT_Fails()
        {
            // Setup
            var message = new CreateRecipeRequest() { };

            // Do
            var response = await TestClient.PostAsJsonAsync("/api/Recipe", message);

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task CreateNew_Recipe()
        {
            // Setup
            var VerifiedTestClient = TestClient;
            VerifiedTestClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", GeneralContainer.JWT);
            var message = new CreateRecipeRequest()
            {
                Title = "Fried pork in spicy sauce",
                PrepTime = 47,
                Directions = "Do this, then that and finish with the other that...\nThen the food is ready.",
                PhotoLink = "some.image.link.com",
                AuthorId = GuidContainer.AuthorId,
                DifficultyId = GuidContainer.DifficultyId,
                CategoryIds = new Guid[]
                {
                    GuidContainer.CategoryChineseId,
                    GuidContainer.CategoryMeatId
                },
                IngredientIds = new Guid[]
                {
                    GuidContainer.IngredientGarlicId,
                    GuidContainer.IngredientOnionId,
                    GuidContainer.IngredientPorkId,
                    GuidContainer.IngredientSoySauceId
                },
                SpecialToolIds = new Guid[]
                {
                    GuidContainer.ToolSpatulaId,
                    GuidContainer.ToolWokId
                }
            };

            // Do
            var response = await VerifiedTestClient.PostAsJsonAsync("/api/Recipe", message);
            GuidContainer.RecipeId = (await response.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }
    }

    [Collection("Business Scenario")]
    [CollectionDefinition("Business Scenario", DisableParallelization = true)]
    public class Seq_03_Test_CreatedRecipe : IntegrationTestBaseClass
    {
        [Fact]
        public async Task LikeCount_Equals_0()
        {
            // Do
            var response = await TestClient.GetAsync($"/api/Recipe/{GuidContainer.RecipeId}");

            // Assert
            (await response.Content.ReadAsAsync<GetRecipeResponseItem>()).LikeCount.Should().Be(0);
        }

        [Fact]
        public async Task Has_Correct_Author()
        {
            // Do
            var response = await TestClient.GetAsync($"/api/Recipe/{GuidContainer.RecipeId}");

            // Assert
            (await response.Content.ReadAsAsync<GetRecipeResponseItem>()).Author.Id.Should().Be(GuidContainer.AuthorId);
        }

        [Fact]
        public async Task Has_Correct_Difficulty()
        {
            // Do
            var response = await TestClient.GetAsync($"/api/Recipe/{GuidContainer.RecipeId}");

            // Assert
            (await response.Content.ReadAsAsync<GetRecipeResponseItem>()).Difficulty.Id.Should().Be(GuidContainer.DifficultyId);
        }

        [Fact]
        public async Task Has_Correct_Categories()
        {
            // Do
            var response = await TestClient.GetAsync($"/api/Recipe/{GuidContainer.RecipeId}");

            // Assert
            var keys = (await response.Content.ReadAsAsync<GetRecipeResponseItem>()).Categories.Select(i => i.Id);
            keys.Should().Contain(GuidContainer.CategoryChineseId);
            keys.Should().Contain(GuidContainer.CategoryMeatId);
        }

        [Fact]
        public async Task Has_Correct_Ingredients()
        {
            // Do
            var response = await TestClient.GetAsync($"/api/Recipe/{GuidContainer.RecipeId}");

            // Assert
            var keys = (await response.Content.ReadAsAsync<GetRecipeResponseItem>()).Ingredients.Select(i => i.Id);
            keys.Should().Contain(GuidContainer.IngredientGarlicId);
            keys.Should().Contain(GuidContainer.IngredientOnionId);
            keys.Should().Contain(GuidContainer.IngredientPorkId);
            keys.Should().Contain(GuidContainer.IngredientSoySauceId);
        }

        [Fact]
        public async Task Has_Correct_SpecialTools()
        {
            // Do
            var response = await TestClient.GetAsync($"/api/Recipe/{GuidContainer.RecipeId}");

            // Assert
            var keys = (await response.Content.ReadAsAsync<GetRecipeResponseItem>()).SpecialTools.Select(i => i.Id);
            keys.Should().Contain(GuidContainer.ToolSpatulaId);
            keys.Should().Contain(GuidContainer.ToolWokId);
        }
    }

    [Collection("Business Scenario")]
    [CollectionDefinition("Business Scenario", DisableParallelization = true)]
    public class Seq_04_Like_And_Review : IntegrationTestBaseClass
    {
        [Fact]
        public async Task Add_500_to_1999_Likes()
        {
            // Setup
            Random rnd = new Random();
            GeneralContainer.LikeCount = rnd.Next(500, 2000);
            var responses = new HttpResponseMessage[GeneralContainer.LikeCount];


            // Do
            for (int i = 0; i < responses.Length; i++)
            {
                responses[i] = await TestClient.PutAsJsonAsync($"/api/Recipe/Like/{GuidContainer.RecipeId}", "{}");
            }

            // Assert
            responses.Where(r => r.StatusCode != HttpStatusCode.OK).Should().BeEmpty();
            responses.Where(r => r.StatusCode == HttpStatusCode.OK).Should().HaveCount(GeneralContainer.LikeCount);
        }

        [Fact]
        public async Task Add_2_Reviews()
        {
            // Setup
            var reviewNo1 = new CreateReviewRequest()
            {
                Value = "Positive Review",
                ReviewersName = "Biggus Dickus",
                PublishNameConsent = true,
                RecipeId = GuidContainer.RecipeId
            };
            var reviewNo2 = new CreateReviewRequest()
            {
                Value = "Negative Review",
                ReviewersName = "Incontinetia Buttocks",
                PublishNameConsent = true,
                RecipeId = GuidContainer.RecipeId
            };

            // Do
            var responseNo1 = await TestClient.PostAsJsonAsync("/api/Review", reviewNo1);
            var responseNo2 = await TestClient.PostAsJsonAsync("/api/Review", reviewNo2);

            GuidContainer.ReviewNo1Id = (await responseNo1.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;
            GuidContainer.ReviewNo2Id = (await responseNo2.Content.ReadAsAsync<GeneralCreatedIdResponse>()).Id;

            // Assert
            responseNo1.StatusCode.Should().Be(HttpStatusCode.Created);
            responseNo2.StatusCode.Should().Be(HttpStatusCode.Created);
        }
    }

    [Collection("Business Scenario")]
    [CollectionDefinition("Business Scenario", DisableParallelization = true)]
    public class Seq_05_Test_UpdatedRecipe : IntegrationTestBaseClass
    {
        [Fact]
        public async Task Liking_Didnt_Cause_RaceCondition()
        {
            // Do
            var response = await TestClient.GetAsync($"/api/Recipe/{GuidContainer.RecipeId}");

            // Assert
            (await response.Content.ReadAsAsync<GetRecipeResponseItem>()).LikeCount.Should().Be(GeneralContainer.LikeCount);
        }

        [Fact]
        public async Task Has_Correct_Reviews()
        {
            // Do
            var response = await TestClient.GetAsync($"/api/Recipe/{GuidContainer.RecipeId}");

            // Assert
            var keys = (await response.Content.ReadAsAsync<GetRecipeResponseItem>()).Reviews.Select(r => r.Id);
            keys.Should().Contain(GuidContainer.ReviewNo1Id);
            keys.Should().Contain(GuidContainer.ReviewNo2Id);
        }
    }

    [Collection("Business Scenario")]
    [CollectionDefinition("Business Scenario", DisableParallelization = true)]
    public class Seq_98_Delete_Recipe : IntegrationTestBaseClass
    {
        [Fact]
        public async Task Delete_Recipe_Without_JWT_Fails()
        {
            // Do
            var response = await TestClient.DeleteAsync($"/api/Recipe/{GuidContainer.RecipeId}");

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task Delete_Recipe()
        {
            // Setup
            var verifiedTestClient = TestClient;
            verifiedTestClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", GeneralContainer.JWT);

            // Do
            var response = await verifiedTestClient.DeleteAsync($"/api/Recipe/{GuidContainer.RecipeId}");
            var reviewNo1Response = await verifiedTestClient.GetAsync($"/api/Review/{GuidContainer.ReviewNo1Id}");
            var reviewNo2Response = await verifiedTestClient.GetAsync($"/api/Review/{GuidContainer.ReviewNo2Id}");

            // Assert
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
            reviewNo1Response.StatusCode.Should().Be(HttpStatusCode.NotFound);
            reviewNo2Response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }
    }

    [Collection("Business Scenario")]
    [CollectionDefinition("Business Scenario", DisableParallelization = true)]
    public class Seq_99_Delete_Data : IntegrationTestBaseClass
    {
        [Fact]
        public async Task Cleanup()
        {
            // Do
            var deleteAuthorResponse = await TestClient.DeleteAsync($"/api/Author/{GuidContainer.AuthorId}");
            var deleteDifficultyResponse = await TestClient.DeleteAsync($"/api/Difficulty/{GuidContainer.DifficultyId}");
            var deleteCategoryChineseResponse = await TestClient.DeleteAsync($"/api/Category/{GuidContainer.CategoryChineseId}");
            var deleteCategoryMeatResponse = await TestClient.DeleteAsync($"/api/Category/{GuidContainer.CategoryMeatId}");
            var deleteToolSpatulaResponse = await TestClient.DeleteAsync($"/api/SpecialTool/{GuidContainer.ToolSpatulaId}");
            var deleteToolWokResponse = await TestClient.DeleteAsync($"/api/SpecialTool/{GuidContainer.ToolWokId}");
            var deleteIngredientPorkResponse = await TestClient.DeleteAsync($"/api/Ingredient/{GuidContainer.IngredientPorkId}");
            var deleteIngredientGarlicResponse = await TestClient.DeleteAsync($"/api/Ingredient/{GuidContainer.IngredientGarlicId}");
            var deleteIngredientOnionResponse = await TestClient.DeleteAsync($"/api/Ingredient/{GuidContainer.IngredientOnionId}");
            var deleteIngredientSoySauceResponse = await TestClient.DeleteAsync($"/api/Ingredient/{GuidContainer.IngredientSoySauceId}");

            // Assert
            deleteAuthorResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
            deleteDifficultyResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
            deleteCategoryChineseResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
            deleteCategoryMeatResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
            deleteToolSpatulaResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
            deleteToolWokResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
            deleteIngredientPorkResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
            deleteIngredientGarlicResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
            deleteIngredientOnionResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
            deleteIngredientSoySauceResponse.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }
    }
}
