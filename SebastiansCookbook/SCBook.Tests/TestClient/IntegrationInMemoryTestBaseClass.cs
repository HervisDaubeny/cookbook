using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.EntityFrameworkCore;
using SCBook.WebApi;
using System.Net.Http;
using Microsoft.AspNetCore.TestHost;
using System.Linq;

namespace SCBook.Tests
{
    public class IntegrationInMemoryTestBaseClass
    {
        protected readonly HttpClient TestClient;


        public IntegrationInMemoryTestBaseClass()
        {
            var appFactory = new WebApplicationFactory<Startup>()
                .WithWebHostBuilder(builder =>
                {
                    builder.ConfigureTestServices(services =>
                    {
                        // Remove existing DbContext configuration
                        var dbContextOptions = services
                            .SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<Dal.SCBookContext>));
                        if (dbContextOptions != null)
                        {
                            services.Remove(dbContextOptions);
                        }

                        // Remove existing DbContext
                        services.RemoveAll(typeof(Dal.SCBookContext));
                        services.AddDbContext<Dal.SCBookContext>(options =>
                        {
                            options.UseInMemoryDatabase("TestDb");
                        });
                    });
                });

            TestClient = appFactory.CreateClient();
        }
    }
}
