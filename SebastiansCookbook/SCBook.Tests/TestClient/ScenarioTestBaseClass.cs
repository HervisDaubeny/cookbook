﻿using LightBDD.NUnit3;
using SCBook.WebApi;
using System.Net.Http;

namespace SCBook.Tests
{
    public class ScenarioTestBaseClass : FeatureFixture
    {
        protected readonly HttpClient TestClient;

        public ScenarioTestBaseClass()
        {
            var appFactory = new TestingWebApplicationFactory<Startup>() { };

            TestClient = appFactory.CreateClient();
        }
    }
}
