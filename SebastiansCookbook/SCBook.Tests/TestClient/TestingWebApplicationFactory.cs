﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using System;

namespace SCBook.Tests
{
    internal class TestingWebApplicationFactory<TStratup> : WebApplicationFactory<TStratup> where TStratup : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.UseSolutionRelativeContentRoot(AppContext.BaseDirectory);
            builder.UseEnvironment("Testing");
            base.ConfigureWebHost(builder);
        }
    }
}
