using Microsoft.AspNetCore.Mvc.Testing;
using SCBook.WebApi;
using System.Net.Http;

namespace SCBook.Tests
{
    public class IntegrationTestBaseClass
    {
        protected readonly HttpClient TestClient;

        public IntegrationTestBaseClass()
        {
            var appFactory = new TestingWebApplicationFactory<Startup>() { };

            TestClient = appFactory.CreateClient();
        }
    }
}
