﻿using FluentAssertions;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace SCBook.Tests.Basic
{
    public class AuthorControllerTests : IntegrationTestBaseClass
    {
        [Fact]
        public async Task GetAll_Authors()
        {
            // Do
            var response = await TestClient.GetAsync("/api/Author");

            // Assert
            response.StatusCode.Should().Be(System.Net.HttpStatusCode.OK);
            (await response.Content.ReadAsAsync<WebApi.Messages.GetAuthorResponse>()).Collection.Should().NotBeEmpty();
        }
    }
}
