﻿using Microsoft.Extensions.DependencyInjection;
using SCBook.Core.IServices;
using SCBook.Core.Services;

namespace SCBook.WebApi.ConfigureServicesExtensions
{
    public static class CoreServiceRegistrationExtension
    {
        public static IServiceCollection RegisterCoreServices(this IServiceCollection services)
        {
            // Register SCBook.Core services
            services.AddTransient<IRecipeService, RecipeService>();
            services.AddTransient<IAuthorService, AuthorService>();
            services.AddTransient<ISpecialToolService, SpecialToolService>();
            services.AddTransient<IIngredientService, IngredientService>();
            services.AddTransient<ICategoryService, CategoryService>();
            services.AddTransient<IDifficultyService, DifficultyService>();
            services.AddTransient<IReviewService, ReviewService>();

            return services;
        }
    }
}
