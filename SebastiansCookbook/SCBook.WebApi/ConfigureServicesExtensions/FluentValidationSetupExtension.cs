﻿using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using System.Text.Json.Serialization;

namespace SCBook.WebApi.ConfigureServicesExtensions
{
    public static class FluentValidationSetupExtension
    {
        public static IServiceCollection SetupFluentValidation(this IServiceCollection services)
        {
            services
               .AddMvc(setup => { })
               .AddJsonOptions(opts =>
               {
                   var enumConverter = new JsonStringEnumConverter();
                   opts.JsonSerializerOptions.Converters.Add(enumConverter);
               })
               .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddTransient<IValidatorFactory, ServiceProviderValidatorFactory>();

            return services;
        }
    }
}
