﻿using MicroElements.Swashbuckle.FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System;
using System.IO;
using System.Reflection;

namespace SCBook.WebApi.ConfigureServicesExtensions
{
    public static class SwaggerConfigurationExtension
    {
        public static IServiceCollection ConfigureSwagger(this IServiceCollection services)
        {

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "SCBook API",
                    Description = "A simple example ASP.NET Core Web API",
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);

                // Add filters to find example providers.
                c.ExampleFilters();
            });

            // Generate examples using example providers.
            services.AddSwaggerExamplesFromAssemblyOf<Startup>();

            // Generate fluent validation documentation.
            services.AddFluentValidationRulesToSwagger();

            return services;
        }
    }
}
