﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace SCBook.WebApi.ConfigureServicesExtensions
{
    public static class DatabaseConnectionExtension
    {
        public static IServiceCollection ConfigureDatabaseConnection(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment environment)
        {
            services.AddDbContext<SCBook.Dal.SCBookContext>(options =>
            {
                options.EnableSensitiveDataLogging();

                if (environment.IsDevelopment())
                {
                    options.UseLoggerFactory(LoggerFactory.Create(builder => builder.AddConsole()));
                }

                options
                    .UseNpgsql(configuration.GetConnectionString("ConnectionString"), x => x.MigrationsAssembly("SCBook.Dal.SQL"))
                    .ConfigureWarnings(warnings =>
                    {
                        // Remove warning about redundant indexes. For example for m:n tables and `CreateIndex` methods generated in migrations
                        // which are not needed.
                        warnings.Ignore(Microsoft.EntityFrameworkCore.Diagnostics.CoreEventId.RedundantIndexRemoved);
                    });
            });

            return services;
        }
    }
}
