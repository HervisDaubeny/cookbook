﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace SCBook.WebApi.Middleware
{
    /// <summary>
    /// 
    /// </summary>
    public class EntityFrameworkMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="next"></param>
        public EntityFrameworkMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (SCBook.Core.Exceptions.KeyDoesNotExistException)
            {
                context.Response.StatusCode = StatusCodes.Status404NotFound;
            }
        }
    }
}
