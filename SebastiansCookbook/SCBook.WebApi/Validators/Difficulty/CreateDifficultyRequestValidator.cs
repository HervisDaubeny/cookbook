using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.Difficulty
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateDifficultyRequestValidator : AbstractValidator<CreateDifficultyRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public CreateDifficultyRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(150);
        }
    }
}
