using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.Difficulty
{
    /// <summary>
    /// 
    /// </summary>
    public class UpdateDifficultyRequestValidator : AbstractValidator<UpdateDifficultyRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public UpdateDifficultyRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(150);
            RuleFor(p => p.Active).NotNull();
        }
    }
}
