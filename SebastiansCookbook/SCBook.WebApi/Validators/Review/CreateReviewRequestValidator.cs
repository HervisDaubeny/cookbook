using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.Review
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateReviewRequestValidator : AbstractValidator<CreateReviewRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public CreateReviewRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(500);
            RuleFor(p => p.ReviewersName).NotNull().NotEmpty().MaximumLength(150);
        }
    }
}
