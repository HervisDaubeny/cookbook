using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.Category
{
    /// <summary>
    /// 
    /// </summary>
    public class UpdateCategoryRequestValidator : AbstractValidator<UpdateCategoryRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public UpdateCategoryRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(150);
            RuleFor(p => p.Active).NotNull();
        }
    }
}
