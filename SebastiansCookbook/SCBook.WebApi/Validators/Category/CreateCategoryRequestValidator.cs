using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.Category
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateCategoryRequestValidator : AbstractValidator<CreateCategoryRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public CreateCategoryRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(150);
        }
    }
}
