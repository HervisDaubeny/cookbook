﻿using FluentValidation;
using SCBook.Core.Enums;
using SCBook.WebApi.Messages;

namespace SCBook.WebApi.Validators.Recipe
{
    public class FiltrationAndPaginationRequestValidator
    {
        /// <summary>
        /// 
        /// </summary>
        public class CreateRecipeRequestValidator : AbstractValidator<FiltrationAndPaginationRequest>
        {
            /// <summary>
            /// 
            /// </summary>
            public CreateRecipeRequestValidator()
            {
                RuleFor(p => p.FilterType).NotNull();
                RuleFor(p => p.FilterValue).Empty().When(p => p.FilterType == RecipeFilter.None);
                RuleFor(p => p.FilterValue).NotEmpty().Unless(p => p.FilterType == RecipeFilter.None);
                RuleFor(p => p.Offset).GreaterThanOrEqualTo(0);
                RuleFor(p => p.Take).GreaterThanOrEqualTo(1).LessThanOrEqualTo(100);
            }
        }
    }
}
