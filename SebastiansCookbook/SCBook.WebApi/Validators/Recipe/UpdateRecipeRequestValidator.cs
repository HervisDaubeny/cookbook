using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.Recipe
{
    /// <summary>
    /// 
    /// </summary>
    public class UpdateRecipeRequestValidator : AbstractValidator<UpdateRecipeRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public UpdateRecipeRequestValidator()
        {
            RuleFor(p => p.Title).NotNull().NotEmpty().MaximumLength(150);
            RuleFor(p => p.PrepTime).NotNull();
            RuleFor(p => p.Directions).NotNull().NotEmpty().MaximumLength(2000);
            RuleFor(p => p.PhotoLink).NotEmpty().MaximumLength(2000);
            RuleFor(p => p.AuthorId).NotNull();
            RuleFor(p => p.DifficultyId).NotNull();
        }
    }
}
