using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.Ingredient
{
    /// <summary>
    /// 
    /// </summary>
    public class UpdateIngredientRequestValidator : AbstractValidator<UpdateIngredientRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public UpdateIngredientRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(150);
            RuleFor(p => p.Active).NotNull();
        }
    }
}
