using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.Ingredient
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateIngredientRequestValidator : AbstractValidator<CreateIngredientRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public CreateIngredientRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(150);
        }
    }
}
