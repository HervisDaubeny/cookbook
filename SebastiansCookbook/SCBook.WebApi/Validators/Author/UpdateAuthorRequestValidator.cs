using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.Author
{
    /// <summary>
    /// 
    /// </summary>
    public class UpdateAuthorRequestValidator : AbstractValidator<UpdateAuthorRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public UpdateAuthorRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(150);
            RuleFor(p => p.Active).NotNull();
        }
    }
}
