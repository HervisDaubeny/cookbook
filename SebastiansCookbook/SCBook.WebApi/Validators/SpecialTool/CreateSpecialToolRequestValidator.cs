using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.SpecialTool
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateSpecialToolRequestValidator : AbstractValidator<CreateSpecialToolRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public CreateSpecialToolRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(150);
        }
    }
}
