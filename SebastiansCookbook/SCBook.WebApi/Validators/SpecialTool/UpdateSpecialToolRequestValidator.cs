using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.SpecialTool
{
    /// <summary>
    /// 
    /// </summary>
    public class UpdateSpecialToolRequestValidator : AbstractValidator<UpdateSpecialToolRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public UpdateSpecialToolRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(150);
            RuleFor(p => p.Active).NotNull();
        }
    }
}
