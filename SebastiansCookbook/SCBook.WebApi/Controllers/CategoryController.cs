using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using SCBook.WebApi.Messages;
using SCBook.WebApi.Mappers;
using Microsoft.AspNetCore.Http;
using SCBook.Core.IServices;

namespace SCBook.WebApi.Controllers
{
    /// <summary>
    /// Controller defining Category catalog endpoints.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ILogger<CategoryController> _logger;
        private readonly ICategoryService _categoryService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="categoryService"></param>
        public CategoryController(ILogger<CategoryController> logger, ICategoryService categoryService)
        {
            _logger = logger;
            _categoryService = categoryService;
        }

        /// <summary>
        /// Get list of existing Category catalog values.
        /// </summary>
        /// <returns>Https response</returns>
        /// <response code="200">Category catalog entry list successfully found</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetCategoryResponse))]
        public IActionResult GetCategoryList()
        {
            var categoryList = _categoryService.GetCategoryList().ToMessage();
            return Ok(categoryList);
        }

        /// <summary>
        /// Get value of Category catalog entry specified by id.
        /// </summary>
        /// <param name="id">Category identification</param>
        /// <returns>Http response</returns>
        /// <response code="200">Category successfully found</response>
        /// <response code="404">Errors occurred during lookup process</response>
        [HttpGet("{id:Guid}", Name = nameof(GetCategoryItem))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetCategoryResponseItem))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult GetCategoryItem([FromRoute] Guid id)
        {
            var category = _categoryService.GetCategory(id).ToMessage();
            return Ok(category);
        }

        /// <summary>
        /// Create new Category catalog entry.
        /// </summary>
        /// <param name="requestBody">DTO for Category catalog entry creation</param>
        /// <returns>Http response</returns>
        /// <response code="201">Category catalog entry value successfully created</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(GeneralCreatedIdResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        public IActionResult CreateCategoryItem([FromBody] CreateCategoryRequest requestBody)
        {
            Guid createdId = _categoryService.CreateCategory(requestBody.ToDto());
            var url = Url.Link(nameof(GetCategoryItem), new { id = createdId });
            return Created(url, new GeneralCreatedIdResponse() { Id = createdId });
        }

        /// <summary>
        /// Update value of Category catalog entry.
        /// </summary>
        /// <param name="id">Category catalog entry identification</param>
        /// <param name="requestBody">DTO for Category catalog entry update</param>
        /// <returns>Http response</returns>
        /// <response code="200">Category catalog entry value successfully updated</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpPut("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult UpdateCategoryItem([FromRoute] Guid id, [FromBody] UpdateCategoryRequest requestBody)
        {
            _categoryService.UpdateCategory(id, requestBody.ToDto());
            return Ok();
        }

        /// <summary>
        /// Delete specified Category catalog entry.
        /// </summary>
        /// <param name="id">Category catalog entry specification</param>
        /// <returns>Http response</returns>
        /// <response code="204">Category catalog entry value successfully deleted</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpDelete("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult DeleteCategoryItem([FromRoute] Guid id)
        {
            _categoryService.DeleteCategory(id);
            return NoContent();
        }
    }
}
