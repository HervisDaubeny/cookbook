using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using SCBook.WebApi.Messages;
using SCBook.WebApi.Mappers;
using Microsoft.AspNetCore.Http;
using SCBook.Core.IServices;

namespace SCBook.WebApi.Controllers
{
    /// <summary>
    /// Controller defining SpecialTool catalog endpoints.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class SpecialToolController : ControllerBase
    {
        private readonly ILogger<SpecialToolController> _logger;
        private readonly ISpecialToolService _specialToolService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="specialToolService"></param>
        public SpecialToolController(ILogger<SpecialToolController> logger, ISpecialToolService specialToolService)
        {
            _logger = logger;
            _specialToolService = specialToolService;
        }

        /// <summary>
        /// Get list of existing SpecialTool catalog values.
        /// </summary>
        /// <returns>Https response</returns>
        /// <response code="200">SpecialTool catalog entry list successfully found</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetSpecialToolResponse))]
        public IActionResult GetSpecialToolList()
        {
            var specialToolList = _specialToolService.GetSpecialToolList().ToMessage();
            return Ok(specialToolList);
        }

        /// <summary>
        /// Get value of SpecialTool catalog entry specified by id.
        /// </summary>
        /// <param name="id">SpecialTool identification</param>
        /// <returns>Http response</returns>
        /// <response code="200">SpecialTool successfully found</response>
        /// <response code="404">Errors occurred during lookup process</response>
        [HttpGet("{id:Guid}", Name = nameof(GetSpecialToolItem))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetSpecialToolResponseItem))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult GetSpecialToolItem([FromRoute] Guid id)
        {
            var specialTool = _specialToolService.GetSpecialTool(id).ToMessage();
            return Ok(specialTool);
        }

        /// <summary>
        /// Create new SpecialTool catalog entry.
        /// </summary>
        /// <param name="requestBody">DTO for SpecialTool catalog entry creation</param>
        /// <returns>Http response</returns>
        /// <response code="201">SpecialTool catalog entry value successfully created</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(GeneralCreatedIdResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        public IActionResult CreateSpecialToolItem([FromBody] CreateSpecialToolRequest requestBody)
        {
            Guid createdId = _specialToolService.CreateSpecialTool(requestBody.ToDto());
            var url = Url.Link(nameof(GetSpecialToolItem), new { id = createdId });
            return Created(url, new GeneralCreatedIdResponse() { Id = createdId });
        }

        /// <summary>
        /// Update value of SpecialTool catalog entry.
        /// </summary>
        /// <param name="id">SpecialTool catalog entry identification</param>
        /// <param name="requestBody">DTO for SpecialTool catalog entry update</param>
        /// <returns>Http response</returns>
        /// <response code="200">SpecialTool catalog entry value successfully updated</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpPut("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult UpdateSpecialToolItem([FromRoute] Guid id, [FromBody] UpdateSpecialToolRequest requestBody)
        {
            _specialToolService.UpdateSpecialTool(id, requestBody.ToDto());
            return Ok();
        }

        /// <summary>
        /// Delete specified SpecialTool catalog entry.
        /// </summary>
        /// <param name="id">SpecialTool catalog entry specification</param>
        /// <returns>Http response</returns>
        /// <response code="204">SpecialTool catalog entry value successfully deleted</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpDelete("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult DeleteSpecialToolItem([FromRoute] Guid id)
        {
            _specialToolService.DeleteSpecialTool(id);
            return NoContent();
        }
    }
}
