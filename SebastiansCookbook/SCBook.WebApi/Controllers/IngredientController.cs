using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using SCBook.WebApi.Messages;
using SCBook.WebApi.Mappers;
using Microsoft.AspNetCore.Http;
using SCBook.Core.IServices;

namespace SCBook.WebApi.Controllers
{
    /// <summary>
    /// Controller defining Ingredient catalog endpoints.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class IngredientController : ControllerBase
    {
        private readonly ILogger<IngredientController> _logger;
        private readonly IIngredientService _ingredientService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="ingredientService"></param>
        public IngredientController(ILogger<IngredientController> logger, IIngredientService ingredientService)
        {
            _logger = logger;
            _ingredientService = ingredientService;
        }

        /// <summary>
        /// Get list of existing Ingredient catalog values.
        /// </summary>
        /// <returns>Https response</returns>
        /// <response code="200">Ingredient catalog entry list successfully found</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetIngredientResponse))]
        public IActionResult GetIngredientList()
        {
            var ingredientList = _ingredientService.GetIngredientList().ToMessage();
            return Ok(ingredientList);
        }

        /// <summary>
        /// Get value of Ingredient catalog entry specified by id.
        /// </summary>
        /// <param name="id">Ingredient identification</param>
        /// <returns>Http response</returns>
        /// <response code="200">Ingredient successfully found</response>
        /// <response code="404">Errors occurred during lookup process</response>
        [HttpGet("{id:Guid}", Name = nameof(GetIngredientItem))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetIngredientResponseItem))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult GetIngredientItem([FromRoute] Guid id)
        {
            var ingredient = _ingredientService.GetIngredient(id).ToMessage();
            return Ok(ingredient);
        }

        /// <summary>
        /// Create new Ingredient catalog entry.
        /// </summary>
        /// <param name="requestBody">DTO for Ingredient catalog entry creation</param>
        /// <returns>Http response</returns>
        /// <response code="201">Ingredient catalog entry value successfully created</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(GeneralCreatedIdResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        public IActionResult CreateIngredientItem([FromBody] CreateIngredientRequest requestBody)
        {
            Guid createdId = _ingredientService.CreateIngredient(requestBody.ToDto());
            var url = Url.Link(nameof(GetIngredientItem), new { id = createdId });
            return Created(url, new GeneralCreatedIdResponse() { Id = createdId });
        }

        /// <summary>
        /// Update value of Ingredient catalog entry.
        /// </summary>
        /// <param name="id">Ingredient catalog entry identification</param>
        /// <param name="requestBody">DTO for Ingredient catalog entry update</param>
        /// <returns>Http response</returns>
        /// <response code="200">Ingredient catalog entry value successfully updated</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpPut("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult UpdateIngredientItem([FromRoute] Guid id, [FromBody] UpdateIngredientRequest requestBody)
        {
            _ingredientService.UpdateIngredient(id, requestBody.ToDto());
            return Ok();
        }

        /// <summary>
        /// Delete specified Ingredient catalog entry.
        /// </summary>
        /// <param name="id">Ingredient catalog entry specification</param>
        /// <returns>Http response</returns>
        /// <response code="204">Ingredient catalog entry value successfully deleted</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpDelete("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult DeleteIngredientItem([FromRoute] Guid id)
        {
            _ingredientService.DeleteIngredient(id);
            return NoContent();
        }
    }
}
