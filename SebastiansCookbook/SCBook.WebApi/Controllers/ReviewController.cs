﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCBook.Core.IServices;
using SCBook.WebApi.Mappers;
using SCBook.WebApi.Messages;
using System;

namespace SCBook.WebApi.Controllers
{
    /// <summary>
    /// Controller defining Review entity endpoints.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class ReviewController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        /// <summary>
        /// Get list of existing Reviews for Recipe specified by id.
        /// </summary>
        /// /// <param name="id"></param>
        /// <returns>Http response</returns>
        /// <response code="200">Review list successfully found</response>
        /// <response code="404">Errors occurred during lookup process</response>
        [HttpGet("ByRecipe/{id:Guid}", Name = nameof(GetReviewList))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetReviewResponse))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult GetReviewList([FromRoute] Guid id)
        {
            var reviewList = _reviewService.GetReviewList(id).ToMessages();
            return Ok(reviewList);
        }

        /// <summary>
        /// Get value of Review specified by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Http response</returns>
        /// <response code="200">Review successfully found</response>
        /// <response code="404">Errors occurred during lookup process</response>
        [HttpGet("{id:Guid}", Name = nameof(GetReviewItem))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetReviewResponseItem))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult GetReviewItem([FromRoute] Guid id)
        {
            var review = _reviewService.GetReview(id).ToMessage();
            return Ok(review);
        }

        /// <summary>
        /// Create new Review.
        /// </summary>
        /// <param name="requestBody"></param>
        /// <returns>Http response</returns>
        /// <response code="201">Review entity successfully created</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(GeneralCreatedIdResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        public IActionResult CreateReview([FromBody] CreateReviewRequest requestBody)
        {
            Guid createdId = _reviewService.CreateReview(requestBody.ToDto());
            var url = Url.Link(nameof(GetReviewItem), new { id = createdId });
            return Created(url, new GeneralCreatedIdResponse() { Id = createdId });
        }

        /// <summary>
        /// Delete Review specified by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Http response</returns>
        /// <response code="204">Review entity successfully deleted</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpDelete("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult DeleteReview([FromRoute] Guid id)
        {
            _reviewService.DeleteReview(id);
            return NoContent();
        }
    }
}
