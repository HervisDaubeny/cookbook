using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using SCBook.WebApi.Messages;
using SCBook.WebApi.Mappers;
using Microsoft.AspNetCore.Http;
using SCBook.Core.IServices;

namespace SCBook.WebApi.Controllers
{
    /// <summary>
    /// Controller defining Difficulty catalog endpoints.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class DifficultyController : ControllerBase
    {
        private readonly ILogger<DifficultyController> _logger;
        private readonly IDifficultyService _difficultyService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="difficultyService"></param>
        public DifficultyController(ILogger<DifficultyController> logger, IDifficultyService difficultyService)
        {
            _logger = logger;
            _difficultyService = difficultyService;
        }

        /// <summary>
        /// Get list of existing Difficulty catalog values.
        /// </summary>
        /// <returns>Https response</returns>
        /// <response code="200">Difficulty catalog entry list successfully found</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetDifficultyResponse))]
        public IActionResult GetDifficultyList()
        {
            var difficultyList = _difficultyService.GetDifficultyList().ToMessage();
            return Ok(difficultyList);
        }

        /// <summary>
        /// Get value of Difficulty catalog entry specified by id.
        /// </summary>
        /// <param name="id">Difficulty identification</param>
        /// <returns>Http response</returns>
        /// <response code="200">Difficulty successfully found</response>
        /// <response code="404">Errors occurred during lookup process</response>
        [HttpGet("{id:Guid}", Name = nameof(GetDifficultyItem))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetDifficultyResponseItem))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult GetDifficultyItem([FromRoute] Guid id)
        {
            var difficulty = _difficultyService.GetDifficulty(id).ToMessage();
            return Ok(difficulty);
        }

        /// <summary>
        /// Create new Difficulty catalog entry.
        /// </summary>
        /// <param name="requestBody">DTO for Difficulty catalog entry creation</param>
        /// <returns>Http response</returns>
        /// <response code="201">Difficulty catalog entry value successfully created</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(GeneralCreatedIdResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        public IActionResult CreateDifficultyItem([FromBody] CreateDifficultyRequest requestBody)
        {
            Guid createdId = _difficultyService.CreateDifficulty(requestBody.ToDto());
            var url = Url.Link(nameof(GetDifficultyItem), new { id = createdId });
            return Created(url, new GeneralCreatedIdResponse() { Id = createdId });
        }

        /// <summary>
        /// Update value of Difficulty catalog entry.
        /// </summary>
        /// <param name="id">Difficulty catalog entry identification</param>
        /// <param name="requestBody">DTO for Difficulty catalog entry update</param>
        /// <returns>Http response</returns>
        /// <response code="200">Difficulty catalog entry value successfully updated</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpPut("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult UpdateDifficultyItem([FromRoute] Guid id, [FromBody] UpdateDifficultyRequest requestBody)
        {
            _difficultyService.UpdateDifficulty(id, requestBody.ToDto());
            return Ok();
        }

        /// <summary>
        /// Delete specified Difficulty catalog entry.
        /// </summary>
        /// <param name="id">Difficulty catalog entry specification</param>
        /// <returns>Http response</returns>
        /// <response code="204">Difficulty catalog entry value successfully deleted</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpDelete("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult DeleteDifficultyItem([FromRoute] Guid id)
        {
            _difficultyService.DeleteDifficulty(id);
            return NoContent();
        }
    }
}
