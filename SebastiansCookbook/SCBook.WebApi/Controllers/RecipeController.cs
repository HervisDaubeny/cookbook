﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SCBook.Core.IServices;
using SCBook.WebApi.Authentication;
using SCBook.WebApi.Mappers;
using SCBook.WebApi.Messages;
using System;

namespace SCBook.WebApi.Controllers
{
    /// <summary>
    /// Controller defining Recipe entity endpoints.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class RecipeController : ControllerBase
    {
        private readonly IRecipeService _recipeService;

        public RecipeController(IRecipeService recipeService)
        {
            _recipeService = recipeService;
        }

        /// <summary>
        /// Get list of existing Recipes.
        /// </summary>
        /// <returns>Http response</returns>
        /// <response code="200">Recipe list successfully found</response>
        /// <response code="400">Filtration is invalid</response>
        /// <response code="400">Pagination is invalid</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetRecipeResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        public IActionResult GetRecipeList([FromQuery]FiltrationAndPaginationRequest filtrationAndPagination)
        {
            var recipeList = _recipeService.GetRecipeList(filtrationAndPagination.ToDto()).ToMessage();
            return Ok(recipeList);
        }

        /// <summary>
        /// Get value of Recipe specified by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Http response</returns>
        /// <response code="200">Recipe successfully found</response>
        /// <response code="404">Errors occurred during lookup process</response>
        [HttpGet("{id:Guid}", Name = nameof(GetRecipeItem))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetRecipeResponseItem))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult GetRecipeItem([FromRoute] Guid id)
        {
            var recipe = _recipeService.GetRecipe(id).ToMessage();
            return Ok(recipe);
        }

        /// <summary>
        /// Create new Recipe.
        /// </summary>
        /// <param name="requestBody"></param>
        /// <returns>Http response</returns>
        /// <response code="201">Recipe entity successfully created</response>
        /// <response code="401">Authentication is necessary to create Recipe</response>
        /// <response code="403">Administrator rights are required to create Recipe</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        [HttpPost]
        [Authorize(Roles = UserRoles.Admin)]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(GeneralCreatedIdResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public IActionResult CreateRecipe([FromBody] CreateRecipeRequest requestBody)
        {
            Guid createdId = _recipeService.CreateRecipe(requestBody.ToDto());
            var url = Url.Link(nameof(GetRecipeItem), new { id = createdId });
            return Created(url, new GeneralCreatedIdResponse() { Id = createdId });
        }

        /// <summary>
        /// Update Recipe specified by id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="requestBody"></param>
        /// <returns>Http response</returns>
        /// <response code="200">Recipe entity successfully updated</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        /// <response code="401">Authentication is necessary to update Recipe</response>
        /// <response code="403">Administrator rights are required to update Recipe</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpPut("{id:Guid}")]
        [Authorize(Roles = UserRoles.Admin)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult UpdateRecipe([FromRoute] Guid id, [FromBody] UpdateRecipeRequest requestBody)
        {
            _recipeService.UpdateRecipe(id, requestBody.ToDto());
            return Ok();
        }

        /// <summary>
        /// Add like to Recipe specified by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Http response</returns>
        /// <response code="200">Recipe entity successfully updated</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpPut("Like/{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult LikeRecipe([FromRoute] Guid id)
        {
            _recipeService.LikeRecipe(id);
            return Ok();
        }

        /// <summary>
        /// Delete Recipe specified by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Http response</returns>
        /// <response code="204">Recipe entity successfully deleted</response>
        /// <response code="401">Authentication is necessary to delete Recipe</response>
        /// <response code="403">Administrator rights are required to delete Recipe</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpDelete("{id:Guid}")]
        [Authorize(Roles = UserRoles.Admin)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult DeleteRecipe([FromRoute] Guid id)
        {
            _recipeService.DeleteRecipe(id);
            return NoContent();
        }
    }
}
