﻿using SCBook.Core.DTOs.Review;
using SCBook.WebApi.Messages;
using System.Collections.Generic;
using System.Linq;
using System;

namespace SCBook.WebApi.Mappers
{
    /// <summary>
    /// Extenstion methods for ReviewMappers
    /// </summary>
    public static class ReviewMapperExtensions
    {
        /// <summary>
        /// Create CreateReviewDto dto from CreateReviewRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static CreateReviewDto ToDto(this CreateReviewRequest request)
        {
            return new CreateReviewDto()
            {
                Value = request.Value,
                ReviewersName = request.ReviewersName,
                TimeOfReview = DateTimeOffset.Now,
                PublishNameConsent = request.PublishNameConsent,
                RecipeId = request.RecipeId,
            };
        }

        /// <summary>
        /// Create GetReviewResponse message collection from GetReviewDetailDto collection
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static GetReviewResponse ToMessages(this IEnumerable<GetReviewDetailDto> dtos)
        {
            return new GetReviewResponse()
            {
                Collection = dtos.Select(d => d.ToMessage()).ToArray()
            };
        }

        /// <summary>
        /// Create GetReviewResponseItem message from GetReviewDetailDto dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static GetReviewResponseItem ToMessage(this GetReviewDetailDto dto)
        {
            return new GetReviewResponseItem()
            {
                Id = dto.Id,
                Value = dto.Value,
                ReviewersName = dto.RecipeName,
                TimeOfReview = dto.TimeOfReview,
                PublishNameConsent = dto.PublishNameConsent,
                RecipeId = dto.RecipeId,
                RecipeName = dto.RecipeName,
            };
        }
    }
}
