using System.Collections.Generic;
using System.Linq;
using SCBook.Core.DTOs.Category;
using SCBook.WebApi.Messages;

namespace SCBook.WebApi.Mappers
{
    /// <summary>
    /// Extension methods for CategoryMappers
    /// </summary>
    public static class CategoryMapperExtension
    {
        /// <summary>
        /// Create UpdateCategoryDto from UpdateCategoryRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static UpdateCategoryDto ToDto(this UpdateCategoryRequest request)
        {
            return new UpdateCategoryDto() { Value = request.Value, Active = request.Active!.Value };
        }

        /// <summary>
        /// Create CreateCategoryDto from CreateCategoryRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static CreateCategoryDto ToDto(this CreateCategoryRequest request)
        {
            return new CreateCategoryDto() { Value = request.Value };
        }

        /// <summary>
        /// Create GetCategoryResponseItem message from GetCategoryDto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static GetCategoryResponseItem ToMessage(this GetCategoryDto dto)
        {
            return new GetCategoryResponseItem() { Id = dto.Id, Value = dto.Value, Active = dto.Active };
        }

        /// <summary>
        /// Create GetCategoryResponse message from collection of GetCategoryDto
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static GetCategoryResponse ToMessage(this IEnumerable<GetCategoryDto> dtos)
        {
            var message = new GetCategoryResponse();
            message.Collection = dtos.Select(d => d.ToMessage()).ToArray();
            return (message);
        }
    }
}
