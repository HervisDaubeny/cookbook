using System.Collections.Generic;
using System.Linq;
using SCBook.Core.DTOs.Ingredient;
using SCBook.WebApi.Messages;

namespace SCBook.WebApi.Mappers
{
    /// <summary>
    /// Extension methods for IngredientMappers
    /// </summary>
    public static class IngredientMapperExtension
    {
        /// <summary>
        /// Create UpdateIngredientDto from UpdateIngredientRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static UpdateIngredientDto ToDto(this UpdateIngredientRequest request)
        {
            return new UpdateIngredientDto() { Value = request.Value, Active = request.Active!.Value };
        }

        /// <summary>
        /// Create CreateIngredientDto from CreateIngredientRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static CreateIngredientDto ToDto(this CreateIngredientRequest request)
        {
            return new CreateIngredientDto() { Value = request.Value };
        }

        /// <summary>
        /// Create GetIngredientResponseItem message from GetIngredientDto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static GetIngredientResponseItem ToMessage(this GetIngredientDto dto)
        {
            return new GetIngredientResponseItem() { Id = dto.Id, Value = dto.Value, Active = dto.Active };
        }

        /// <summary>
        /// Create GetIngredientResponse message from collection of GetIngredientDto
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static GetIngredientResponse ToMessage(this IEnumerable<GetIngredientDto> dtos)
        {
            var message = new GetIngredientResponse();
            message.Collection = dtos.Select(d => d.ToMessage()).ToArray();
            return (message);
        }
    }
}
