﻿using SCBook.Core.DTOs.Recipe;
using SCBook.WebApi.Messages;
using SCBook.WebApi.Extensions;
using System.Linq;

namespace SCBook.WebApi.Mappers
{
    /// <summary>
    /// Extenstion methods for RecipeMappers
    /// </summary>
    public static class RecipeMapperExtensions
    {
        /// <summary>
        /// Create CreateRecipeDto dto from CreateRecipeRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static CreateRecipeDto ToDto(this CreateRecipeRequest request)
        {
            return new CreateRecipeDto()
            {
                Title = request.Title,
                PrepTime = request.PrepTime,
                Directions = request.Directions,
                PhotoLink = request.PhotoLink,
                AuthorId = request.AuthorId,
                DifficultyId = request.DifficultyId,
                CategoryIds = request.CategoryIds,
                IngredientIds = request.IngredientIds,
                SpecialToolIds = request.SpecialToolIds,
            };
        }

        /// <summary>
        /// Create UpdateRecipeDto dto from UpdateRecipeRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static UpdateRecipeDto ToDto(this UpdateRecipeRequest request)
        {
            return new UpdateRecipeDto()
            {
                Title = request.Title,
                PrepTime = request.PrepTime,
                Directions = request.Directions,
                PhotoLink = request.PhotoLink,
                AuthorId = request.AuthorId,
                DifficultyId = request.DifficultyId,
                CategoryIds = request.CategoryIds,
                IngredientIds = request.IngredientIds,
                SpecialToolIds = request.SpecialToolIds,
            };
        }

        /// <summary>
        /// Create GetRecipeResponse message collection from GetRecipeDto collection
        /// </summary>
        /// <param name="listDto"></param>
        /// <returns></returns>
        public static GetRecipeResponse ToMessage(this GetRecipeListDto listDto)
        {
            return new GetRecipeResponse()
            {
                Filtered = listDto.Filtered,
                Total = listDto.Total,
                Collection = listDto.Collection.Select(d => d.ToMessage()).ToArray()
            };
        }

        /// <summary>
        /// Create GetRecipeResponseItem message from GetRecipeDto dto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static GetRecipeResponseItem ToMessage(this GetRecipeDto dto)
        {
            return new GetRecipeResponseItem()
            {
                Id = dto.Id,
                Title = dto.Title,
                PrepTime = dto.PrepTime,
                Directions = dto.Directions,
                LikeCount = dto.LikeCount,
                PhotoLink = dto.PhotoLink,
                Author = dto.Author.ToWebApiCatalogPairDto(),
                Difficulty = dto.Difficulty.ToWebApiCatalogPairDto(),
                Categories = dto.Categories.ToWebApiCatalogPairDtos(),
                Ingredients = dto.Ingredients.ToWebApiCatalogPairDtos(),
                SpecialTools = dto.SpecialTools.ToWebApiCatalogPairDtos(),
                Reviews = dto.Reviews.ToWebApiReviewDtos(),
            };
        }

        /// <summary>
        /// Create FiltrationAndPaginationDto dto from FiltrationAndPaginationRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static FiltrationAndPaginationDto ToDto(this FiltrationAndPaginationRequest request)
        {
            return new FiltrationAndPaginationDto()
            {
                FilterType = request.FilterType.Value,
                FilterValue = request.FilterValue,
                Offset = request.Offset,
                Take = request.Take
            };
        }
    }
}
