using System.Collections.Generic;
using System.Linq;
using SCBook.Core.DTOs.SpecialTool;
using SCBook.WebApi.Messages;

namespace SCBook.WebApi.Mappers
{
    /// <summary>
    /// Extension methods for SpecialToolMappers
    /// </summary>
    public static class SpecialToolMapperExtension
    {
        /// <summary>
        /// Create UpdateSpecialToolDto from UpdateSpecialToolRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static UpdateSpecialToolDto ToDto(this UpdateSpecialToolRequest request)
        {
            return new UpdateSpecialToolDto() { Value = request.Value, Active = request.Active!.Value };
        }

        /// <summary>
        /// Create CreateSpecialToolDto from CreateSpecialToolRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static CreateSpecialToolDto ToDto(this CreateSpecialToolRequest request)
        {
            return new CreateSpecialToolDto() { Value = request.Value };
        }

        /// <summary>
        /// Create GetSpecialToolResponseItem message from GetSpecialToolDto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static GetSpecialToolResponseItem ToMessage(this GetSpecialToolDto dto)
        {
            return new GetSpecialToolResponseItem() { Id = dto.Id, Value = dto.Value, Active = dto.Active };
        }

        /// <summary>
        /// Create GetSpecialToolResponse message from collection of GetSpecialToolDto
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static GetSpecialToolResponse ToMessage(this IEnumerable<GetSpecialToolDto> dtos)
        {
            var message = new GetSpecialToolResponse();
            message.Collection = dtos.Select(d => d.ToMessage()).ToArray();
            return (message);
        }
    }
}
