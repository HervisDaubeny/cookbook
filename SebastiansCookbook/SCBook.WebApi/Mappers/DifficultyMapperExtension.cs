using System.Collections.Generic;
using System.Linq;
using SCBook.Core.DTOs.Difficulty;
using SCBook.WebApi.Messages;

namespace SCBook.WebApi.Mappers
{
    /// <summary>
    /// Extension methods for DifficultyMappers
    /// </summary>
    public static class DifficultyMapperExtension
    {
        /// <summary>
        /// Create UpdateDifficultyDto from UpdateDifficultyRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static UpdateDifficultyDto ToDto(this UpdateDifficultyRequest request)
        {
            return new UpdateDifficultyDto() { Value = request.Value, Active = request.Active!.Value };
        }

        /// <summary>
        /// Create CreateDifficultyDto from CreateDifficultyRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static CreateDifficultyDto ToDto(this CreateDifficultyRequest request)
        {
            return new CreateDifficultyDto() { Value = request.Value };
        }

        /// <summary>
        /// Create GetDifficultyResponseItem message from GetDifficultyDto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static GetDifficultyResponseItem ToMessage(this GetDifficultyDto dto)
        {
            return new GetDifficultyResponseItem() { Id = dto.Id, Value = dto.Value, Active = dto.Active };
        }

        /// <summary>
        /// Create GetDifficultyResponse message from collection of GetDifficultyDto
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static GetDifficultyResponse ToMessage(this IEnumerable<GetDifficultyDto> dtos)
        {
            var message = new GetDifficultyResponse();
            message.Collection = dtos.Select(d => d.ToMessage()).ToArray();
            return (message);
        }
    }
}
