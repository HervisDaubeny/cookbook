using SCBook.WebApi.Messages;
using SCBook.WebApi.Messages.SpecialTool.Fakers;
using Swashbuckle.AspNetCore.Filters;

namespace SCBook.WebApi.ExampleProviders
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateSpecialToolRequestExampleProvider : IExamplesProvider<CreateSpecialToolRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public CreateSpecialToolRequest GetExamples() => new CreateSpecialToolRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateSpecialToolRequestExampleProvider : IExamplesProvider<UpdateSpecialToolRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public UpdateSpecialToolRequest GetExamples() => new UpdateSpecialToolRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetSpecialToolResponseItemExampleProvider : IExamplesProvider<GetSpecialToolResponseItem>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetSpecialToolResponseItem GetExamples() => new GetSpecialToolResponseItemFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetSpecialToolResponseExampleProvider : IExamplesProvider<GetSpecialToolResponse>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetSpecialToolResponse GetExamples() => new GetSpecialToolResponseFaker().Generate();
    }
}
