using SCBook.WebApi.Messages;
using SCBook.WebApi.Messages.Author.Fakers;
using Swashbuckle.AspNetCore.Filters;

namespace SCBook.WebApi.ExampleProviders
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateAuthorRequestExampleProvider : IExamplesProvider<CreateAuthorRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public CreateAuthorRequest GetExamples() => new CreateAuthorRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateAuthorRequestExampleProvider : IExamplesProvider<UpdateAuthorRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public UpdateAuthorRequest GetExamples() => new UpdateAuthorRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetAuthorResponseItemExampleProvider : IExamplesProvider<GetAuthorResponseItem>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetAuthorResponseItem GetExamples() => new GetAuthorResponseItemFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetAuthorResponseExampleProvider : IExamplesProvider<GetAuthorResponse>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetAuthorResponse GetExamples() => new GetAuthorResponseFaker().Generate();
    }
}
