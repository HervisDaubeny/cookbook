using SCBook.WebApi.Messages;
using SCBook.WebApi.Messages.Recipe.Fakers;
using Swashbuckle.AspNetCore.Filters;

namespace SCBook.WebApi.ExampleProviders
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateRecipeRequestExampleProvider : IExamplesProvider<CreateRecipeRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public CreateRecipeRequest GetExamples() => new CreateRecipeRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateRecipeRequestExampleProvider : IExamplesProvider<UpdateRecipeRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public UpdateRecipeRequest GetExamples() => new UpdateRecipeRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetRecipeResponseItemExampleProvider : IExamplesProvider<GetRecipeResponseItem>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetRecipeResponseItem GetExamples() => new GetRecipeResponseItemFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetRecipeResponseExampleProvider : IExamplesProvider<GetRecipeResponse>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetRecipeResponse GetExamples() => new GetRecipeResponseFaker().Generate();
    }
}
