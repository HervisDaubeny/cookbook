using SCBook.WebApi.Messages;
using SCBook.WebApi.Messages.Category.Fakers;
using Swashbuckle.AspNetCore.Filters;

namespace SCBook.WebApi.ExampleProviders
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateCategoryRequestExampleProvider : IExamplesProvider<CreateCategoryRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public CreateCategoryRequest GetExamples() => new CreateCategoryRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateCategoryRequestExampleProvider : IExamplesProvider<UpdateCategoryRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public UpdateCategoryRequest GetExamples() => new UpdateCategoryRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetCategoryResponseItemExampleProvider : IExamplesProvider<GetCategoryResponseItem>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetCategoryResponseItem GetExamples() => new GetCategoryResponseItemFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetCategoryResponseExampleProvider : IExamplesProvider<GetCategoryResponse>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetCategoryResponse GetExamples() => new GetCategoryResponseFaker().Generate();
    }
}
