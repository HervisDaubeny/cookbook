using SCBook.WebApi.Messages;
using SCBook.WebApi.Messages.Ingredient.Fakers;
using Swashbuckle.AspNetCore.Filters;

namespace SCBook.WebApi.ExampleProviders
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateIngredientRequestExampleProvider : IExamplesProvider<CreateIngredientRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public CreateIngredientRequest GetExamples() => new CreateIngredientRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateIngredientRequestExampleProvider : IExamplesProvider<UpdateIngredientRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public UpdateIngredientRequest GetExamples() => new UpdateIngredientRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetIngredientResponseItemExampleProvider : IExamplesProvider<GetIngredientResponseItem>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetIngredientResponseItem GetExamples() => new GetIngredientResponseItemFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetIngredientResponseExampleProvider : IExamplesProvider<GetIngredientResponse>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetIngredientResponse GetExamples() => new GetIngredientResponseFaker().Generate();
    }
}
