using SCBook.WebApi.Messages;
using SCBook.WebApi.Messages.Difficulty.Fakers;
using Swashbuckle.AspNetCore.Filters;

namespace SCBook.WebApi.ExampleProviders
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateDifficultyRequestExampleProvider : IExamplesProvider<CreateDifficultyRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public CreateDifficultyRequest GetExamples() => new CreateDifficultyRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class UpdateDifficultyRequestExampleProvider : IExamplesProvider<UpdateDifficultyRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public UpdateDifficultyRequest GetExamples() => new UpdateDifficultyRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetDifficultyResponseItemExampleProvider : IExamplesProvider<GetDifficultyResponseItem>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetDifficultyResponseItem GetExamples() => new GetDifficultyResponseItemFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetDifficultyResponseExampleProvider : IExamplesProvider<GetDifficultyResponse>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetDifficultyResponse GetExamples() => new GetDifficultyResponseFaker().Generate();
    }
}
