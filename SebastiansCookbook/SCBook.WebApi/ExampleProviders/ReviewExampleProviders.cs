using SCBook.WebApi.Messages;
using SCBook.WebApi.Messages.Review.Fakers;
using Swashbuckle.AspNetCore.Filters;

namespace SCBook.WebApi.ExampleProviders
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateReviewRequestExampleProvider : IExamplesProvider<CreateReviewRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public CreateReviewRequest GetExamples() => new CreateReviewRequestFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetReviewResponseItemExampleProvider : IExamplesProvider<GetReviewResponseItem>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetReviewResponseItem GetExamples() => new GetReviewResponseItemFaker().Generate();
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetReviewResponseExampleProvider : IExamplesProvider<GetReviewResponse>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public GetReviewResponse GetExamples() => new GetReviewResponseFaker().Generate();
    }
}
