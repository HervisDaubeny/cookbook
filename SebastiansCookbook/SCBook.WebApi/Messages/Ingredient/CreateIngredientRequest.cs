// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for Ingredient catalog creating request
    /// </summary>
    public class CreateIngredientRequest
    {
        /// <summary>
        /// Value of catalog Ingredient
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
