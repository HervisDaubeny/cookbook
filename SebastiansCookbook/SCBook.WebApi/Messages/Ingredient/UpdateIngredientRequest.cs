// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for Ingredient catalog updating request
    /// </summary>
    public class UpdateIngredientRequest
    {
        /// <summary>
        /// Value of catalog Ingredient
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Ingredient catalog value
        /// </summary>
        public bool? Active { get; set; } = default!;
    }
}
