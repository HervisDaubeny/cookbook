﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Ingredient.Fakers
{
    public class CreateIngredientRequestFaker : IFaker<CreateIngredientRequest>
    {
        public Bogus.Faker<CreateIngredientRequest> BogusGenerator { get; }

        public CreateIngredientRequestFaker()
        {
            BogusGenerator = new AutoFaker<CreateIngredientRequest>()
                .RuleFor(p => p.Value, f => f.Lorem.Word()); //TODO: select a valid faker
        }

        public CreateIngredientRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
