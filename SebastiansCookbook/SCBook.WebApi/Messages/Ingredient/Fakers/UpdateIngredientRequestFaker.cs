﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Ingredient.Fakers
{
    public class UpdateIngredientRequestFaker : IFaker<UpdateIngredientRequest>
    {

        public Bogus.Faker<UpdateIngredientRequest> BogusGenerator { get; }

        public UpdateIngredientRequestFaker()
        {
            BogusGenerator = new AutoFaker<UpdateIngredientRequest>()
                .RuleFor(p => p.Value, f => f.Lorem.Word())
                .RuleFor(p => p.Active, f => f.Random.Bool());

        }

        public UpdateIngredientRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
