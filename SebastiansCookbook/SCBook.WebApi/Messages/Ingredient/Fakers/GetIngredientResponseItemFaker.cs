﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Ingredient.Fakers
{
    public class GetIngredientResponseItemFaker : IFaker<GetIngredientResponseItem>
    {
        public Bogus.Faker<GetIngredientResponseItem> BogusGenerator { get; }

        public GetIngredientResponseItemFaker()
        {
            BogusGenerator = new AutoFaker<GetIngredientResponseItem>()
                .RuleFor(p => p.Id, f => f.Random.Guid())
                .RuleFor(p => p.Value, f => f.Lorem.Word())
                .RuleFor(p => p.Active, f => f.Random.Bool());
        }

        public GetIngredientResponseItem Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
