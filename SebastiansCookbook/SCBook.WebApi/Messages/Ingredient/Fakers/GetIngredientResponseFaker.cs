﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Ingredient.Fakers
{
    public class GetIngredientResponseFaker : IFaker<GetIngredientResponse>
    {
        public Bogus.Faker<GetIngredientResponse> BogusGenerator { get; }

        public GetIngredientResponseFaker()
        {
            var from = new GetIngredientResponseItemFaker();
            BogusGenerator = new AutoFaker<GetIngredientResponse>()
                .RuleFor(
                    p => p.Collection,
                    f => f.GenerateMany(from));
        }

        public GetIngredientResponse Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
