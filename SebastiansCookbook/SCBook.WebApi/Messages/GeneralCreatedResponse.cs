﻿using System;

namespace SCBook.WebApi.Messages
{
    public class GeneralCreatedIdResponse
    {
        public Guid Id { get; set; }
    }
}
