// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for Author catalog creating request
    /// </summary>
    public class CreateAuthorRequest
    {
        /// <summary>
        /// Value of catalog Author
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
