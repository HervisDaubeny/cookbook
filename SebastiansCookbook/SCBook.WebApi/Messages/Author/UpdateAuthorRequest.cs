// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for Author catalog updating request
    /// </summary>
    public class UpdateAuthorRequest
    {
        /// <summary>
        /// Value of catalog Author
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Author catalog value
        /// </summary>
        public bool? Active { get; set; } = default!;
    }
}
