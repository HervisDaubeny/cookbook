﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Author.Fakers
{
    public class GetAuthorResponseFaker : IFaker<GetAuthorResponse>
    {
        public Bogus.Faker<GetAuthorResponse> BogusGenerator { get; }

        public GetAuthorResponseFaker()
        {
            var from = new GetAuthorResponseItemFaker();
            BogusGenerator = new AutoFaker<GetAuthorResponse>()
                .RuleFor(
                    p => p.Collection,
                    f => f.GenerateMany(from));
        }

        public GetAuthorResponse Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
