﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Author.Fakers
{
    public class CreateAuthorRequestFaker : IFaker<CreateAuthorRequest>
    {
        public Bogus.Faker<CreateAuthorRequest> BogusGenerator { get; }

        public CreateAuthorRequestFaker()
        {
            BogusGenerator = new AutoFaker<CreateAuthorRequest>()
                .RuleFor(p => p.Value, f => f.Person.FullName.ToString());
        }

        public CreateAuthorRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
