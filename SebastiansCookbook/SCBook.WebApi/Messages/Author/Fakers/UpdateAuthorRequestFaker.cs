﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Author.Fakers
{
    public class UpdateAuthorRequestFaker : IFaker<UpdateAuthorRequest>
    {

        public Bogus.Faker<UpdateAuthorRequest> BogusGenerator { get; }

        public UpdateAuthorRequestFaker()
        {
            BogusGenerator = new AutoFaker<UpdateAuthorRequest>()
                .RuleFor(p => p.Value, f => f.Person.FullName.ToString())
                .RuleFor(p => p.Active, f => f.Random.Bool());

        }

        public UpdateAuthorRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
