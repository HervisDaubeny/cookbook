﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Author.Fakers
{
    public class GetAuthorResponseItemFaker : IFaker<GetAuthorResponseItem>
    {
        public Bogus.Faker<GetAuthorResponseItem> BogusGenerator { get; }

        public GetAuthorResponseItemFaker()
        {
            BogusGenerator = new AutoFaker<GetAuthorResponseItem>()
                .RuleFor(p => p.Id, f => f.Random.Guid())
                .RuleFor(p => p.Value, f => f.Person.FullName.ToString())
                .RuleFor(p => p.Active, f => f.Random.Bool());
        }

        public GetAuthorResponseItem Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
