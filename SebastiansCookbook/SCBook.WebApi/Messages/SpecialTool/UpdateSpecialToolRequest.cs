// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for SpecialTool catalog updating request
    /// </summary>
    public class UpdateSpecialToolRequest
    {
        /// <summary>
        /// Value of catalog SpecialTool
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of SpecialTool catalog value
        /// </summary>
        public bool? Active { get; set; } = default!;
    }
}
