﻿using AutoBogus;

namespace SCBook.WebApi.Messages.SpecialTool.Fakers
{
    public class GetSpecialToolResponseFaker : IFaker<GetSpecialToolResponse>
    {
        public Bogus.Faker<GetSpecialToolResponse> BogusGenerator { get; }

        public GetSpecialToolResponseFaker()
        {
            var from = new GetSpecialToolResponseItemFaker();
            BogusGenerator = new AutoFaker<GetSpecialToolResponse>()
                .RuleFor(
                    p => p.Collection,
                    f => f.GenerateMany(from));
        }

        public GetSpecialToolResponse Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
