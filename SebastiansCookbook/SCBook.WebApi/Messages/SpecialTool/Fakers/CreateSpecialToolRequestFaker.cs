﻿using AutoBogus;

namespace SCBook.WebApi.Messages.SpecialTool.Fakers
{
    public class CreateSpecialToolRequestFaker : IFaker<CreateSpecialToolRequest>
    {
        public Bogus.Faker<CreateSpecialToolRequest> BogusGenerator { get; }

        public CreateSpecialToolRequestFaker()
        {
            BogusGenerator = new AutoFaker<CreateSpecialToolRequest>()
                .RuleFor(p => p.Value, f => f.Lorem.Word()); //TODO: select a valid faker
        }

        public CreateSpecialToolRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
