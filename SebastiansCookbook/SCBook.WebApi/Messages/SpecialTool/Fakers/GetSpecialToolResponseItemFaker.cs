﻿using AutoBogus;

namespace SCBook.WebApi.Messages.SpecialTool.Fakers
{
    public class GetSpecialToolResponseItemFaker : IFaker<GetSpecialToolResponseItem>
    {
        public Bogus.Faker<GetSpecialToolResponseItem> BogusGenerator { get; }

        public GetSpecialToolResponseItemFaker()
        {
            BogusGenerator = new AutoFaker<GetSpecialToolResponseItem>()
                .RuleFor(p => p.Id, f => f.Random.Guid())
                .RuleFor(p => p.Value, f => f.Lorem.Word())
                .RuleFor(p => p.Active, f => f.Random.Bool());
        }

        public GetSpecialToolResponseItem Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
