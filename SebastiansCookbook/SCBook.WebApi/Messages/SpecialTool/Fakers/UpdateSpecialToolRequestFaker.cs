﻿using AutoBogus;

namespace SCBook.WebApi.Messages.SpecialTool.Fakers
{
    public class UpdateSpecialToolRequestFaker : IFaker<UpdateSpecialToolRequest>
    {

        public Bogus.Faker<UpdateSpecialToolRequest> BogusGenerator { get; }

        public UpdateSpecialToolRequestFaker()
        {
            BogusGenerator = new AutoFaker<UpdateSpecialToolRequest>()
                .RuleFor(p => p.Value, f => f.Lorem.Word())
                .RuleFor(p => p.Active, f => f.Random.Bool());

        }

        public UpdateSpecialToolRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
