using System;

// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Output message for SpecialTool catalog collection
    /// </summary>
    public class GetSpecialToolResponse : BaseGetResponseCollectionMessage<GetSpecialToolResponseItem> { }


    /// <summary>
    /// Output message for SpecialTool catalog detail get request
    /// </summary>
    public class GetSpecialToolResponseItem
    {
        /// <summary>
        /// UUID of catalog SpecialTool
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Value of catalog SpecialTool
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of SpecialTool catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
