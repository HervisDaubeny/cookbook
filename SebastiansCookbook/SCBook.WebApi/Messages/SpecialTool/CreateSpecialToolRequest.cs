// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for SpecialTool catalog creating request
    /// </summary>
    public class CreateSpecialToolRequest
    {
        /// <summary>
        /// Value of catalog SpecialTool
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
