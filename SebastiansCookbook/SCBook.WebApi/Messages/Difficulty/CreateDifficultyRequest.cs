// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for Difficulty catalog creating request
    /// </summary>
    public class CreateDifficultyRequest
    {
        /// <summary>
        /// Value of catalog Difficulty
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
