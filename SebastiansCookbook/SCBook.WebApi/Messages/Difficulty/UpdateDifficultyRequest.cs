// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for Difficulty catalog updating request
    /// </summary>
    public class UpdateDifficultyRequest
    {
        /// <summary>
        /// Value of catalog Difficulty
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Difficulty catalog value
        /// </summary>
        public bool? Active { get; set; } = default!;
    }
}
