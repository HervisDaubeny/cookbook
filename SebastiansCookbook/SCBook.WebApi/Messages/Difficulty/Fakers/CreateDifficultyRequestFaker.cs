﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Difficulty.Fakers
{
    public class CreateDifficultyRequestFaker : IFaker<CreateDifficultyRequest>
    {
        public Bogus.Faker<CreateDifficultyRequest> BogusGenerator { get; }

        public CreateDifficultyRequestFaker()
        {
            BogusGenerator = new AutoFaker<CreateDifficultyRequest>()
                .RuleFor(p => p.Value, f => f.Random.Number(1, 5).ToString());
        }

        public CreateDifficultyRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
