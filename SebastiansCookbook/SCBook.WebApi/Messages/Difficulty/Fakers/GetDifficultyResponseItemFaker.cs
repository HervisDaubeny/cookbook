﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Difficulty.Fakers
{
    public class GetDifficultyResponseItemFaker : IFaker<GetDifficultyResponseItem>
    {
        public Bogus.Faker<GetDifficultyResponseItem> BogusGenerator { get; }

        public GetDifficultyResponseItemFaker()
        {
            BogusGenerator = new AutoFaker<GetDifficultyResponseItem>()
                .RuleFor(p => p.Id, f => f.Random.Guid())
                .RuleFor(p => p.Value, f => f.Random.Number(1, 5).ToString())
                .RuleFor(p => p.Active, f => f.Random.Bool());
        }

        public GetDifficultyResponseItem Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
