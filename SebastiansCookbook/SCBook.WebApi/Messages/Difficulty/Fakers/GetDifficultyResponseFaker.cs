﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Difficulty.Fakers
{
    public class GetDifficultyResponseFaker : IFaker<GetDifficultyResponse>
    {
        public Bogus.Faker<GetDifficultyResponse> BogusGenerator { get; }

        public GetDifficultyResponseFaker()
        {
            var from = new GetDifficultyResponseItemFaker();
            BogusGenerator = new AutoFaker<GetDifficultyResponse>()
                .RuleFor(
                    p => p.Collection,
                    f => f.GenerateMany(from));
        }

        public GetDifficultyResponse Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
