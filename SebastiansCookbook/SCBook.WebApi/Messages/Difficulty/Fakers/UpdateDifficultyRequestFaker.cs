﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Difficulty.Fakers
{
    public class UpdateDifficultyRequestFaker : IFaker<UpdateDifficultyRequest>
    {

        public Bogus.Faker<UpdateDifficultyRequest> BogusGenerator { get; }

        public UpdateDifficultyRequestFaker()
        {
            BogusGenerator = new AutoFaker<UpdateDifficultyRequest>()
                .RuleFor(p => p.Value, f => f.Random.Number(1, 5).ToString())
                .RuleFor(p => p.Active, f => f.Random.Bool());

        }

        public UpdateDifficultyRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
