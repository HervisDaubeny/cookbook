using System;

// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Output message for Difficulty catalog collection
    /// </summary>
    public class GetDifficultyResponse : BaseGetResponseCollectionMessage<GetDifficultyResponseItem> { }


    /// <summary>
    /// Output message for Difficulty catalog detail get request
    /// </summary>
    public class GetDifficultyResponseItem
    {
        /// <summary>
        /// UUID of catalog Difficulty
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Value of catalog Difficulty
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Difficulty catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
