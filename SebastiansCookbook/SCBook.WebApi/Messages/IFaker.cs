﻿using System.Collections.Generic;
using System.Linq;

namespace SCBook.WebApi.Messages
{
    public interface IFaker<T>
    {
        public T Generate();
    }
    public static class BaseBogusFakerExtension
    {
        public static IEnumerable<T> GenerateMany<T>(this Bogus.Faker f, IFaker<T> from, int min = 1, int max = 10) where T : class
        {
            return f.Random
                .Bytes(f.Random.Int(min, max)) // Create placeholders to generate array
                .Select(_ => from.Generate()) // Call generate on given faker for each item in the array
                .ToArray(); // Cast resulting Enumerable<T> to T[]
        }
    }
}
