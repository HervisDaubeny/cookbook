﻿using System;

namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Output message for Review entity collection
    /// </summary>
    public class GetReviewResponse : BaseGetResponseCollectionMessage<GetReviewResponseItem> { }

    /// <summary>
    /// Output message for Review entity detail get request
    /// </summary>
    public class GetReviewResponseItem
    {
        /// <summary>
        /// UUID of Review entity
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Value of Review entity
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Name of author of Review entity
        /// </summary>
        public string ReviewersName { get; set; } = default!;

        /// <summary>
        /// Timestamp of Review entity creation
        /// </summary>
        public DateTimeOffset TimeOfReview { get; set; }

        /// <summary>
        /// Consent to publish authors name
        /// </summary>
        public bool PublishNameConsent { get; set; } = default!;

        /// <summary>
        /// Id of Recipe entity that Review is about
        /// </summary>
        public Guid RecipeId { get; set; } = default;

        /// <summary>
        /// Name of Recipe entity that Review is about
        /// </summary>
        public string RecipeName { get; set; } = default;
    }
}
