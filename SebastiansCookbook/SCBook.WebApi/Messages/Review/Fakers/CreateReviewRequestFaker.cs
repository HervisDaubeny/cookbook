﻿namespace SCBook.WebApi.Messages.Review.Fakers
{
    public class CreateReviewRequestFaker : IFaker<CreateReviewRequest>
    {
        public Bogus.Faker<CreateReviewRequest> BogusGenerator { get; }

        public CreateReviewRequestFaker()
        {
            BogusGenerator = new AutoBogus.AutoFaker<CreateReviewRequest>()
                .RuleFor(i => i.Value, f => f.Lorem.Paragraph().ToString())
                .RuleFor(i => i.ReviewersName, f => f.Person.FullName.ToString())
                .RuleFor(i => i.PublishNameConsent, f => f.Random.Bool())
                .RuleFor(i => i.RecipeId, f => f.Random.Guid())
                ;
        }

        public CreateReviewRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
