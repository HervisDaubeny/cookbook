﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Review.Fakers
{
    public class GetReviewResponseItemFaker : IFaker<GetReviewResponseItem>
    {
        public Bogus.Faker<GetReviewResponseItem> BogusGenerator { get; }

        public GetReviewResponseItemFaker()
        {
            BogusGenerator = new AutoFaker<GetReviewResponseItem>()
                .RuleFor(i => i.Id, f => f.Random.Guid())
                .RuleFor(i => i.Value, f => f.Lorem.Paragraph().ToString())
                .RuleFor(i => i.ReviewersName, f => f.Person.FullName.ToString())
                .RuleFor(i => i.TimeOfReview, f => f.Date.Recent(10))
                .RuleFor(i => i.PublishNameConsent, f => f.Random.Bool())
                .RuleFor(i => i.RecipeId, f => f.Random.Guid())
                .RuleFor(i => i.RecipeName, f => f.Lorem.Word().ToString())
                ;
        }

        public GetReviewResponseItem Generate()
        {
            return BogusGenerator.Generate();
        }
    }

    public class GetReviewResponseFaker : IFaker<GetReviewResponse>
    {
        public Bogus.Faker<GetReviewResponse> BogusGenerator { get; }

        public GetReviewResponseFaker()
        {
            var from = new GetReviewResponseItemFaker();
            BogusGenerator = new AutoBogus.AutoFaker<GetReviewResponse>()
                .RuleFor(i => i.Collection, f => f.GenerateMany(from));
        }

        public GetReviewResponse Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
