﻿using System;

namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for Review entity creating request
    /// </summary>
    public class CreateReviewRequest
    {
        /// <summary>
        /// Value of Review entity
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Name of author of Review entity
        /// </summary>
        public string ReviewersName { get; set; } = default!;

        /// <summary>
        /// Consent to publish authors name
        /// </summary>
        public bool PublishNameConsent { get; set; } = default!;

        /// <summary>
        /// Id of Recipe entity that Review is about
        /// </summary>
        public Guid RecipeId { get; set; } = default;
    }
}
