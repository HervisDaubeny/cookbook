﻿using Microsoft.AspNetCore.Mvc;
using SCBook.Core.Enums;

namespace SCBook.WebApi.Messages
{
    public class FiltrationAndPaginationRequest
    {
        /// <summary>
        /// Filter type to use.
        /// </summary>
        [FromQuery]
        public RecipeFilter? FilterType { get; set; } = RecipeFilter.None;

        /// <summary>
        /// Value to filter by
        /// </summary>
        [FromQuery]
        public string? FilterValue { get; set; } = "";

        /// <summary>
        /// Number of results from query to skip
        /// </summary>
        [FromQuery]
        public int Offset { get; set; } = 0;

        /// <summary>
        /// Required number of results after the Offset
        /// </summary>
        [FromQuery]
        public int Take { get; set;} = 10;
    }
}
