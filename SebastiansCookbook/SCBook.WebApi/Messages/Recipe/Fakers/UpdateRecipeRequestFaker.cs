﻿using System;

namespace SCBook.WebApi.Messages.Recipe.Fakers
{
    public class UpdateRecipeRequestFaker : IFaker<UpdateRecipeRequest>
    {
        public Bogus.Faker<UpdateRecipeRequest> BogusGenerator { get; }

        public UpdateRecipeRequestFaker()
        {
            BogusGenerator = new AutoBogus.AutoFaker<UpdateRecipeRequest>()
                .RuleFor(i => i.Title, f => f.Lorem.Word().ToString())
                .RuleFor(i => i.PrepTime, f => f.Date.Timespan().Minutes)
                .RuleFor(i => i.Directions, f => f.Lorem.Paragraph().ToString())
                .RuleFor(i => i.PhotoLink, f => f.Image.PicsumUrl())
                .RuleFor(i => i.AuthorId, f => f.Random.Guid())
                .RuleFor(i => i.DifficultyId, f => f.Random.Guid())
                .RuleFor(i => i.CategoryIds, f => Array.Empty<Guid>())
                .RuleFor(i => i.IngredientIds, f => Array.Empty<Guid>())
                .RuleFor(i => i.SpecialToolIds, f => Array.Empty<Guid>())
                ;
        }

        public UpdateRecipeRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
