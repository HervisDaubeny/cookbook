﻿using System;

namespace SCBook.WebApi.Messages.Recipe.Fakers
{
    public class CreateRecipeRequestFaker : IFaker<CreateRecipeRequest>
    {
        public Bogus.Faker<CreateRecipeRequest> BogusGenerator { get; }

        public CreateRecipeRequestFaker()
        {
            BogusGenerator = new AutoBogus.AutoFaker<CreateRecipeRequest>()
                .RuleFor(i => i.Title, f => f.Lorem.Word().ToString())
                .RuleFor(i => i.PrepTime, f => f.Date.Timespan().Minutes)
                .RuleFor(i => i.Directions, f => f.Lorem.Paragraph().ToString())
                .RuleFor(i => i.PhotoLink, f => f.Image.PicsumUrl())
                .RuleFor(i => i.AuthorId, f => f.Random.Guid())
                .RuleFor(i => i.DifficultyId, f => f.Random.Guid())
                .RuleFor(i => i.CategoryIds, f => Array.Empty<Guid>())
                .RuleFor(i => i.IngredientIds, f => Array.Empty<Guid>())
                .RuleFor(i => i.SpecialToolIds, f => Array.Empty<Guid>())
                ;
        }

        public CreateRecipeRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
