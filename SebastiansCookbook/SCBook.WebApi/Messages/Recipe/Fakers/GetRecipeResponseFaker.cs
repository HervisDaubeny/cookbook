﻿using AutoBogus;
using SCBook.WebApi.Messages.Author.Fakers;
using SCBook.WebApi.Messages.Difficulty.Fakers;
using System;

namespace SCBook.WebApi.Messages.Recipe.Fakers
{
    public class GetRecipeResponseItemFaker : IFaker<GetRecipeResponseItem>
    {
        public Bogus.Faker<GetRecipeResponseItem> BogusGenerator { get; }

        public GetRecipeResponseItemFaker()
        {
            var FakeAuthor = new GetAuthorResponseItemFaker().Generate();
            var AuthorCatalogPairDto = new WebApi.DTOs.GetCatalogPairDto { Id = FakeAuthor.Id, Value = FakeAuthor.Value };

            var FakeDifficulty = new GetDifficultyResponseItemFaker().Generate();
            var DifficultyCatalogPairDto = new WebApi.DTOs.GetCatalogPairDto { Id = FakeDifficulty.Id, Value = FakeDifficulty.Value };
            
            BogusGenerator = new AutoFaker<GetRecipeResponseItem>()
                .RuleFor(i => i.Title, f => f.Lorem.Word().ToString())
                .RuleFor(i => i.PrepTime, f => f.Date.Timespan().Minutes)
                .RuleFor(i => i.Directions, f => f.Lorem.Paragraph().ToString())
                .RuleFor(i => i.LikeCount, f => f.Random.Number(1000))
                .RuleFor(i => i.PhotoLink, f => f.Image.PicsumUrl())
                .RuleFor(i => i.Author, _ => AuthorCatalogPairDto)
                .RuleFor(i => i.Difficulty, _ => DifficultyCatalogPairDto)
                .RuleFor(i => i.Categories, f => Array.Empty<WebApi.DTOs.GetCatalogPairDto>())
                .RuleFor(i => i.Ingredients, f => Array.Empty<WebApi.DTOs.GetCatalogPairDto>())
                .RuleFor(i => i.SpecialTools, f => Array.Empty<WebApi.DTOs.GetCatalogPairDto>())
                .RuleFor(i => i.Reviews, f => Array.Empty<WebApi.DTOs.GetReviewDto>())
                ;
        }

        public GetRecipeResponseItem Generate()
        {
            return BogusGenerator.Generate();
        }
    }

    public class GetRecipeResponseFaker : IFaker<GetRecipeResponse>
    {
        public Bogus.Faker<GetRecipeResponse> BogusGenerator { get; }

        public GetRecipeResponseFaker()
        {
            var from = new GetRecipeResponseItemFaker();
            BogusGenerator = new AutoBogus.AutoFaker<GetRecipeResponse>()
                .RuleFor(i => i.Collection, f => f.GenerateMany(from))
                .RuleFor(i => i.Filtered, f => f.Random.Number())
                .RuleFor(i => i.Total, f => f.Random.Number());
        }

        public GetRecipeResponse Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
