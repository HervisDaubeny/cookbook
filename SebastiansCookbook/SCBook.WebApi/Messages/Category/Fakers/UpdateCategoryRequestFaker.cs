﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Category.Fakers
{
    public class UpdateCategoryRequestFaker : IFaker<UpdateCategoryRequest>
    {

        public Bogus.Faker<UpdateCategoryRequest> BogusGenerator { get; }

        public UpdateCategoryRequestFaker()
        {
            BogusGenerator = new AutoFaker<UpdateCategoryRequest>()
                .RuleFor(p => p.Value, f => f.Lorem.Word())
                .RuleFor(p => p.Active, f => f.Random.Bool());

        }

        public UpdateCategoryRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
