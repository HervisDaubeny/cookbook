﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Category.Fakers
{
    public class CreateCategoryRequestFaker : IFaker<CreateCategoryRequest>
    {
        public Bogus.Faker<CreateCategoryRequest> BogusGenerator { get; }

        public CreateCategoryRequestFaker()
        {
            BogusGenerator = new AutoFaker<CreateCategoryRequest>()
                .RuleFor(p => p.Value, f => f.Lorem.Word()); //TODO: select a valid faker
        }

        public CreateCategoryRequest Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
