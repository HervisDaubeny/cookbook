﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Category.Fakers
{
    public class GetCategoryResponseItemFaker : IFaker<GetCategoryResponseItem>
    {
        public Bogus.Faker<GetCategoryResponseItem> BogusGenerator { get; }

        public GetCategoryResponseItemFaker()
        {
            BogusGenerator = new AutoFaker<GetCategoryResponseItem>()
                .RuleFor(p => p.Id, f => f.Random.Guid())
                .RuleFor(p => p.Value, f => f.Lorem.Word())
                .RuleFor(p => p.Active, f => f.Random.Bool());
        }

        public GetCategoryResponseItem Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
