﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Category.Fakers
{
    public class GetCategoryResponseFaker : IFaker<GetCategoryResponse>
    {
        public Bogus.Faker<GetCategoryResponse> BogusGenerator { get; }

        public GetCategoryResponseFaker()
        {
            var from = new GetCategoryResponseItemFaker();
            BogusGenerator = new AutoFaker<GetCategoryResponse>()
                .RuleFor(
                    p => p.Collection,
                    f => f.GenerateMany(from));
        }

        public GetCategoryResponse Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
