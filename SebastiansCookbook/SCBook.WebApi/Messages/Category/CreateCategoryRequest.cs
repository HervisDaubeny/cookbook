// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for Category catalog creating request
    /// </summary>
    public class CreateCategoryRequest
    {
        /// <summary>
        /// Value of catalog Category
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
