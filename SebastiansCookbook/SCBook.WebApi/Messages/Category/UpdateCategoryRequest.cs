// ReSharper disable once CheckNamespace
namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Input message for Category catalog updating request
    /// </summary>
    public class UpdateCategoryRequest
    {
        /// <summary>
        /// Value of catalog Category
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Category catalog value
        /// </summary>
        public bool? Active { get; set; } = default!;
    }
}
