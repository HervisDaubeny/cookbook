﻿namespace SCBook.WebApi.Messages
{
    /// <summary>
    /// Base class to create collection response
    /// </summary>
    /// <typeparam name="T">class to collect</typeparam>
    public class BaseGetResponseCollectionMessage<T> where T : class
    {
        /// <summary>
        /// The collection
        /// </summary>
        public T[] Collection { get; set; } = default!;
    }
}
