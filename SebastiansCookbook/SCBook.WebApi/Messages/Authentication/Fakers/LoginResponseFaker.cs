﻿using AutoBogus;

namespace SCBook.WebApi.Messages.Authentication.Fakers
{
    public class LoginResponseFaker : IFaker<LoginResponse>
    {
        public Bogus.Faker<LoginResponse> BogusGenerator { get; }

        public LoginResponseFaker()
        {
            BogusGenerator = new AutoFaker<LoginResponse>()
                .RuleFor(p => p.Token, f => f.Random.Guid().ToString())
                .RuleFor(p => p.Expiration, f => f.Date.Future());
        }

        public LoginResponse Generate()
        {
            return BogusGenerator.Generate();
        }
    }
}
