﻿using System;

namespace SCBook.WebApi.Messages
{
    public class LoginResponse
    {
        /// <summary>
        /// Authentication token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Expiration of token
        /// </summary>
        public DateTimeOffset Expiration { get; set; }
    }
}
