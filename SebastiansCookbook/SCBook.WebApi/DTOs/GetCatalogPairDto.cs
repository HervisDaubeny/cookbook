﻿using System;

namespace SCBook.WebApi.DTOs
{
    /// <summary>
    /// Output message for catalog get request
    /// </summary>
    public class GetCatalogPairDto
    {
        /// <summary>
        /// UUID of catalog entry
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Value of catalog entry
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
