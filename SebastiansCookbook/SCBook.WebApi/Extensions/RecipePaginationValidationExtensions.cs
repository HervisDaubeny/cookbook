﻿using System;

namespace SCBook.WebApi.Extensions
{
    public static class RecipePaginationValidationExtensions
    {
        public static bool Validate(this Tuple<int, int> pagination)
        {
            if (pagination.Item1 < 10 || pagination.Item1 > 100 || pagination.Item2 < 1)
            {
                return false;
            }

            return true;
        }
    }
}
