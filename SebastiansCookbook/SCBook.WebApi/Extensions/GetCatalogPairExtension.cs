﻿using System.Collections.Generic;
using System.Linq;

namespace SCBook.WebApi.Extensions
{
    public static class GetCatalogPairExtension
    {
        /// <summary>
        /// Explicit conversion from Core.DTOs.GetCatalogPairDto to WebApi.DTOs.GetCatalogPairDto
        /// </summary>
        /// <param name="catalogPairDto">Core.DTOs.GetCatalogPairDto dto to convert</param>
        /// <returns><paramref name="catalogPairDto"/> converted to WebApi.DTOs.GetCatalogPairDto</returns>
        public static SCBook.WebApi.DTOs.GetCatalogPairDto ToWebApiCatalogPairDto(this Core.DTOs.GetCatalogPairDto catalogPairDto)
        {
            return new WebApi.DTOs.GetCatalogPairDto { Id = catalogPairDto.Id, Value = catalogPairDto.Value };
        }

        /// <summary>
        /// Explicit conversion from WebApi.DTOs.GetCatalogPairDto to Core.DTOs.GetCatalogPairDto
        /// </summary>
        /// <param name="catalogPairDto">WebApi.DTOs.GetCatalogPairDto dto to convert</param>
        /// <returns><paramref name="catalogPairDto"/> converted to Core.DTOs.GetCatalogPairDto</returns>
        public static Core.DTOs.GetCatalogPairDto ToCoreCatalogPairDto(this WebApi.DTOs.GetCatalogPairDto catalogPairDto)
        {
            return new Core.DTOs.GetCatalogPairDto { Id = catalogPairDto.Id, Value = catalogPairDto.Value };
        }

        /// <summary>
        /// Explicit conversion from collection of Core.DTOs.GetCatalogPairDto to collection of WebApi.DTOs.GetCatalogPairDto
        /// </summary>
        /// <param name="catalogPairDtos">Core.DTOs.GetCatalogPairDto dtos collection to convert</param>
        /// <returns><paramref name="catalogPairDtos"/> collection converted to WebApi.DTOs.GetCatalogPairDto[]</returns>
        public static SCBook.WebApi.DTOs.GetCatalogPairDto[] ToWebApiCatalogPairDtos(this IEnumerable<Core.DTOs.GetCatalogPairDto> catalogPairDtos)
        {
            return catalogPairDtos.Select(c => c.ToWebApiCatalogPairDto()).ToArray();
        }

        /// <summary>
        /// Explicit conversion from collection of WebApi.DTOs.GetCatalogPairDto to collection of Core.DTOs.GetCatalogPairDto
        /// </summary>
        /// <param name="catalogPairDtos">WebApi.DTOs.GetCatalogPairDto dtos collection to convert</param>
        /// <returns><paramref name="catalogPairDtos"/> collection converted to Core.DTOs.GetCatalogPairDto[]</returns>
        public static Core.DTOs.GetCatalogPairDto[] ToCoreCatalogPairDtos(this IEnumerable<WebApi.DTOs.GetCatalogPairDto> catalogPairDtos)
        {
            return catalogPairDtos.Select(c => c.ToCoreCatalogPairDto()).ToArray();
        }
    }
}
