﻿using System.Collections.Generic;
using System.Linq;

namespace SCBook.WebApi.Extensions
{
    public static class GetReviewDtoExtension
    {
        /// <summary>
        /// Explicit conversion from Core.DTOs.GetReviewDto to WebApi.DTOs.GetReviewDto
        /// </summary>
        /// <param name="dto">Core.DTOs.GetReviewDto dto to convert</param>
        /// <returns><paramref name="dto"/> converted to WebApi.DTOs.GetReviewDto</returns>
        public static WebApi.DTOs.GetReviewDto ToWebApiReviewDto(this Core.DTOs.GetReviewDto dto)
        {
            return new WebApi.DTOs.GetReviewDto
            {
                Id = dto.Id,
                Value = dto.Value,
                ReviewersName = dto.ReviewersName,
                TimeOfReview = dto.TimeOfReview,
                PublishNameConsent = dto.PublishNameConsent,
            };
        }

        /// <summary>
        /// Explicit conversion from WebApi.DTOs.GetReviewDto to Core.DTOs.GetReviewDto
        /// </summary>
        /// <param name="dto">WebApi.DTOs.GetReviewDto dto to convert</param>
        /// <returns><paramref name="dto"/> converted to Core.DTOs.GetReviewDto</returns>
        public static Core.DTOs.GetReviewDto ToCoreReviewDto(this WebApi.DTOs.GetReviewDto dto)
        {
            return new Core.DTOs.GetReviewDto
            {
                Id = dto.Id,
                Value = dto.Value,
                ReviewersName = dto.ReviewersName,
                TimeOfReview = dto.TimeOfReview,
                PublishNameConsent = dto.PublishNameConsent,
            };
        }

        /// <summary>
        /// Explicit conversion from collection of Core.DTOs.GetReviewDto to collection of WebApi.DTOs.GetReviewDto
        /// </summary>
        /// <param name="reviewDtos">Core.DTOs.GetReviewDto dtos collection to convert</param>
        /// <returns><paramref name="reviewDtos"/> collection converted to WebApi.DTOs.GetReviewDto[]</returns>
        public static WebApi.DTOs.GetReviewDto[] ToWebApiReviewDtos(this IEnumerable<Core.DTOs.GetReviewDto> reviewDtos)
        {
            return reviewDtos.Select(r => r.ToWebApiReviewDto()).ToArray();
        }

        /// <summary>
        /// Explicit conversion from collection of WebApi.DTOs.GetReviewDto to collection of Core.DTOs.GetReviewDto
        /// </summary>
        /// <param name="reviewDtos">WebApi.DTOs.GetReviewDto dtos collection to convert</param>
        /// <returns><paramref name="reviewDtos"/> collection converted to Core.DTOs.GetReviewDto[]</returns>
        public static Core.DTOs.GetReviewDto[] ToCoreReviewDtos(this IEnumerable<WebApi.DTOs.GetReviewDto> reviewDtos)
        {
            return reviewDtos.Select(r => r.ToCoreReviewDto()).ToArray();
        }
    }
}
