﻿using SCBook.Core.Enums;
using System;

namespace SCBook.WebApi.Extensions
{
    public static class RecipeFilterValidationExtensions
    {
        public static bool Validate(this Tuple<RecipeFilter, string> filter) {
            if (filter.Item1 != RecipeFilter.None && string.IsNullOrEmpty(filter.Item2))
            {
                return false;
            }

            return true;
        }
    }
}
