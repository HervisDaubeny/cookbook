﻿using SCBook.Core.DTOs.Review;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCBook.Core.Mappers
{
    public static class ReviewMapperExtension
    {
        public static DTOs.Review.GetReviewDetailDto ToDto(this Dal.Entities.Review recipe)
        {
            return new DTOs.Review.GetReviewDetailDto()
            {
                Id = recipe.Id,
                Value = recipe.Value,
                ReviewersName = recipe.ReviewersName,
                TimeOfReview = recipe.TimeOfReview,
                PublishNameConsent = recipe.PublishNameConsent,
                RecipeId = recipe.RecipeId,
                RecipeName = recipe.Recipe.Title,
            };
        }

        public static IEnumerable<Core.DTOs.Review.GetReviewDetailDto> ToDtos(this IEnumerable<Dal.Entities.Review> recipes)
        {
            return recipes.Select(r => r.ToDto()).ToArray();
        }

        public static Dal.Entities.Review ToEntity(this CreateReviewDto dto, Guid recipeId)
        {
            return new Dal.Entities.Review()
            {
                Id = recipeId,
                Value = dto.Value,
                ReviewersName = dto.ReviewersName,
                TimeOfReview = dto.TimeOfReview,
                PublishNameConsent = dto.PublishNameConsent,
                RecipeId = dto.RecipeId,
            };
        }
    }
}
