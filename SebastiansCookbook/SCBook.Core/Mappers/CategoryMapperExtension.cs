using SCBook.Core.DTOs.Category;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCBook.Core.Mappers
{
    internal static class CategoryMapperExtension
    {
        public static DTOs.Category.GetCategoryDto ToDto(this Dal.Catalogs.Category Category)
        {
            return new GetCategoryDto() { Id = Category.Id, Value = Category.Value, Active = Category.Active };
        }

        public static IEnumerable<Core.DTOs.Category.GetCategoryDto> ToDtos (this IEnumerable<Dal.Catalogs.Category> Categorys)
        {
            return Categorys.Select(g => g.ToDto()).ToArray();
        }

        public static Dal.Catalogs.Category ToCatalog(this CreateCategoryDto dto, Guid categoryId)
        {
            return new Dal.Catalogs.Category()
            {
                Id = categoryId,
                Value = dto.Value,
                Active = true
            };
        }

        public static Dal.Catalogs.Category UpdateWith(this Dal.Catalogs.Category category, UpdateCategoryDto dto)
        {
            category.Value = dto.Value;
            category.Active = dto.Active;

            return category;
        }
    }
}
