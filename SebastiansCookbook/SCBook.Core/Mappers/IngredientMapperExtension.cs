using SCBook.Core.DTOs.Ingredient;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCBook.Core.Mappers
{
    internal static class IngredientMapperExtension
    {
        public static DTOs.Ingredient.GetIngredientDto ToDto(this Dal.Catalogs.Ingredient Ingredient)
        {
            return new GetIngredientDto() { Id = Ingredient.Id, Value = Ingredient.Value, Active = Ingredient.Active };
        }

        public static IEnumerable<Core.DTOs.Ingredient.GetIngredientDto> ToDtos (this IEnumerable<Dal.Catalogs.Ingredient> Ingredients)
        {
            return Ingredients.Select(g => g.ToDto()).ToArray();
        }

        public static Dal.Catalogs.Ingredient ToCatalog(this CreateIngredientDto dto, Guid ingredientId)
        {
            return new Dal.Catalogs.Ingredient()
            {
                Id = ingredientId,
                Value = dto.Value,
                Active = true
            };
        }

        public static Dal.Catalogs.Ingredient UpdateWith(this Dal.Catalogs.Ingredient ingredient, UpdateIngredientDto dto)
        {
            ingredient.Value = dto.Value;
            ingredient.Active = dto.Active;

            return ingredient;
        }
    }
}
