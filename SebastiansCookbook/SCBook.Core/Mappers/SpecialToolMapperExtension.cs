using SCBook.Core.DTOs.SpecialTool;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCBook.Core.Mappers
{
    internal static class SpecialToolMapperExtension
    {
        public static DTOs.SpecialTool.GetSpecialToolDto ToDto(this Dal.Catalogs.SpecialTool SpecialTool)
        {
            return new GetSpecialToolDto() { Id = SpecialTool.Id, Value = SpecialTool.Value, Active = SpecialTool.Active };
        }

        public static IEnumerable<Core.DTOs.SpecialTool.GetSpecialToolDto> ToDtos (this IEnumerable<Dal.Catalogs.SpecialTool> SpecialTools)
        {
            return SpecialTools.Select(g => g.ToDto()).ToArray();
        }

        public static Dal.Catalogs.SpecialTool ToCatalog(this CreateSpecialToolDto dto, Guid specialToolId)
        {
            return new Dal.Catalogs.SpecialTool()
            {
                Id = specialToolId,
                Value = dto.Value,
                Active = true
            };
        }

        public static Dal.Catalogs.SpecialTool UpdateWith(this Dal.Catalogs.SpecialTool specialTool, UpdateSpecialToolDto dto)
        {
            specialTool.Value = dto.Value;
            specialTool.Active = dto.Active;

            return specialTool;
        }
    }
}
