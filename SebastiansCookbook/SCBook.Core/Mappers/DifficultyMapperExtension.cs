using SCBook.Core.DTOs.Difficulty;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCBook.Core.Mappers
{
    internal static class DifficultyMapperExtension
    {
        public static DTOs.Difficulty.GetDifficultyDto ToDto(this Dal.Catalogs.Difficulty Difficulty)
        {
            return new GetDifficultyDto() { Id = Difficulty.Id, Value = Difficulty.Value, Active = Difficulty.Active };
        }

        public static IEnumerable<Core.DTOs.Difficulty.GetDifficultyDto> ToDtos (this IEnumerable<Dal.Catalogs.Difficulty> Difficultys)
        {
            return Difficultys.Select(g => g.ToDto()).ToArray();
        }

        public static Dal.Catalogs.Difficulty ToCatalog(this CreateDifficultyDto dto, Guid difficultyId)
        {
            return new Dal.Catalogs.Difficulty()
            {
                Id = difficultyId,
                Value = dto.Value,
                Active = true
            };
        }

        public static Dal.Catalogs.Difficulty UpdateWith(this Dal.Catalogs.Difficulty difficulty, UpdateDifficultyDto dto)
        {
            difficulty.Value = dto.Value;
            difficulty.Active = dto.Active;

            return difficulty;
        }
    }
}
