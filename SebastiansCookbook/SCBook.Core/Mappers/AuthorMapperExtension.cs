using SCBook.Core.DTOs.Author;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCBook.Core.Mappers
{
    internal static class AuthorMapperExtension
    {
        public static DTOs.Author.GetAuthorDto ToDto(this Dal.Catalogs.Author Author)
        {
            return new GetAuthorDto() { Id = Author.Id, Value = Author.Value, Active = Author.Active };
        }

        public static IEnumerable<Core.DTOs.Author.GetAuthorDto> ToDtos (this IEnumerable<Dal.Catalogs.Author> Authors)
        {
            return Authors.Select(g => g.ToDto()).ToArray();
        }

        public static Dal.Catalogs.Author ToCatalog(this CreateAuthorDto dto, Guid authorId)
        {
            return new Dal.Catalogs.Author()
            {
                Id = authorId,
                Value = dto.Value,
                Active = true
            };
        }

        public static Dal.Catalogs.Author UpdateWith(this Dal.Catalogs.Author author, UpdateAuthorDto dto)
        {
            author.Value = dto.Value;
            author.Active = dto.Active;

            return author;
        }
    }
}
