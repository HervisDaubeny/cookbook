﻿using SCBook.Core.DTOs.Recipe;
using SCBook.Core.Extensions;
using SCBook.Dal.Entities;
using SCBook.Dal.JoiningEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCBook.Core.Mappers
{
    public static class RecipeMapperExtension
    {
        public static DTOs.Recipe.GetRecipeDto ToDto(this Dal.Entities.Recipe recipe)
        {
            return new DTOs.Recipe.GetRecipeDto()
            {
                Id = recipe.Id,
                Title = recipe.Title,
                PrepTime = recipe.PrepTime,
                Directions = recipe.Directions,
                LikeCount = recipe.LikeCount,
                PhotoLink = recipe.PhotoLink,
                Author = recipe.Author.ToCatalogPairDto(),
                Difficulty = recipe.Difficulty.ToCatalogPairDto(),
                Categories = recipe.RecipeCategories.ToCategoryCatalogPairDtos(),
                Ingredients = recipe.RecipeIngredients.ToIngredientCatalogPairDtos(),
                SpecialTools = recipe.RecipeSpecialTools.ToSpecialToolCatalogPairDtos(),
                Reviews = recipe.Reviews.ToGetReviewDtos()
            };
        }

        public static IEnumerable<Core.DTOs.Recipe.GetRecipeDto> ToDtos(this IEnumerable<Dal.Entities.Recipe> recipes)
        {
            return recipes.Select(r => r.ToDto()).ToArray();
        }

        public static GetRecipeListDto ToListDto(this IEnumerable<GetRecipeDto> recipeDtos, int filtered, int total)
        {
            return new GetRecipeListDto()
            {
                Filtered = filtered,
                Total = total,
                Collection = recipeDtos.ToArray()
            };
        }

        public static Dal.Entities.Recipe ToEntity(this CreateRecipeDto dto, Guid recipeId)
        {
            return new Dal.Entities.Recipe()
            {
                Id = recipeId,
                Title = dto.Title,
                PrepTime = dto.PrepTime,
                Directions = dto.Directions,
                LikeCount = 0,
                PhotoLink = dto.PhotoLink,
                AuthorId = dto.AuthorId,
                DifficultyId = dto.DifficultyId,
                RecipeCategories = dto.CategoryIds.Select(cId => new RecipeCategory { RecipeId = recipeId, CategoryId = cId }).ToHashSet(),
                RecipeIngredients = dto.IngredientIds.Select(iId => new RecipeIngredient { RecipeId = recipeId, IngredientId = iId }).ToHashSet(),
                RecipeSpecialTools = dto.SpecialToolIds.Select(stId => new RecipeSpecialTool { RecipeId = recipeId, SpecialToolId = stId }).ToHashSet(),
            };
        }

        public static Dal.Entities.Recipe UpdateWith(this Dal.Entities.Recipe recipe, UpdateRecipeDto dto)
        {
            recipe.Title = dto.Title;
            recipe.PrepTime = dto.PrepTime;
            recipe.Directions = dto.Directions;
            recipe.PhotoLink = dto.PhotoLink;
            recipe.AuthorId = dto.AuthorId;
            recipe.DifficultyId = dto.DifficultyId;

            // Update Recipe - Category relations
            recipe.UpdateJoiningRelations(
                r => r.RecipeCategories,
                dto.CategoryIds
                    .Where(ci => !recipe.RecipeCategories
                        .Select(rc => rc.CategoryId)
                        .Contains(ci)),
                gds => gds
                    .Select(c => new RecipeCategory { CategoryId = c, RecipeId = recipe.Id }),
                recipe.RecipeCategories
                    .Where(rc => !dto.CategoryIds
                        .Contains(rc.CategoryId)));

            // Update Recipe - Ingredient relations
            recipe.UpdateJoiningRelations(
                r => r.RecipeIngredients,
                dto.IngredientIds
                    .Where(ii => !recipe.RecipeIngredients
                        .Select(ri => ri.IngredientId)
                        .Contains(ii)),
                gds => gds
                    .Select(i => new RecipeIngredient { IngredientId = i, RecipeId = recipe.Id }),
                recipe.RecipeIngredients
                    .Where(ri => !dto.IngredientIds
                        .Contains(ri.IngredientId)));

            // Update Recipe - SpecialTool relations
            recipe.UpdateJoiningRelations(
                r => r.RecipeSpecialTools,
                dto.SpecialToolIds
                    .Where(sti => !recipe.RecipeSpecialTools
                        .Select(rsti => rsti.SpecialToolId)
                        .Contains(sti)),
                gds => gds
                    .Select(st => new RecipeSpecialTool { SpecialToolId = st, RecipeId = recipe.Id }),
                recipe.RecipeSpecialTools
                    .Where(rsti => !dto.SpecialToolIds
                        .Contains(rsti.SpecialToolId)));

            return recipe;
        }
    }
}
