using System;
using System.Collections.Generic;
using SCBook.Core.DTOs.SpecialTool;

namespace SCBook.Core.IServices
{
    public interface ISpecialToolService
    {
        public GetSpecialToolDto GetSpecialTool(Guid id);
        public IEnumerable<GetSpecialToolDto> GetSpecialToolList();
        public void UpdateSpecialTool(Guid id, UpdateSpecialToolDto updateDto);
        public Guid CreateSpecialTool(CreateSpecialToolDto createDto);
        public void DeleteSpecialTool(Guid id);
    }
}
