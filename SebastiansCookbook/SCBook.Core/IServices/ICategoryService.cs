using System;
using System.Collections.Generic;
using SCBook.Core.DTOs.Category;

namespace SCBook.Core.IServices
{
    public interface ICategoryService
    {
        public GetCategoryDto GetCategory(Guid id);
        public IEnumerable<GetCategoryDto> GetCategoryList();
        public void UpdateCategory(Guid id, UpdateCategoryDto updateDto);
        public Guid CreateCategory(CreateCategoryDto createDto);
        public void DeleteCategory(Guid id);
    }
}
