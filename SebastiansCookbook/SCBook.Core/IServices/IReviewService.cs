﻿using SCBook.Core.DTOs.Review;
using System;
using System.Collections.Generic;

namespace SCBook.Core.IServices
{
    public interface IReviewService
    {
        public Guid CreateReview(CreateReviewDto createDto);
        public IEnumerable<GetReviewDetailDto> GetReviewList(Guid recipeId);
        public GetReviewDetailDto GetReview(Guid reviewId);
        public void DeleteReview(Guid id);
    }
}
