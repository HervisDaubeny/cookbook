using System;
using System.Collections.Generic;
using SCBook.Core.DTOs.Difficulty;

namespace SCBook.Core.IServices
{
    public interface IDifficultyService
    {
        public GetDifficultyDto GetDifficulty(Guid id);
        public IEnumerable<GetDifficultyDto> GetDifficultyList();
        public void UpdateDifficulty(Guid id, UpdateDifficultyDto updateDto);
        public Guid CreateDifficulty(CreateDifficultyDto createDto);
        public void DeleteDifficulty(Guid id);
    }
}
