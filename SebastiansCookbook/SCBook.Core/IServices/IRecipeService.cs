﻿using SCBook.Core.DTOs.Recipe;
using System;

namespace SCBook.Core.IServices
{
    public interface IRecipeService
    {
        public GetRecipeListDto GetRecipeList(FiltrationAndPaginationDto filterPageDto);
        public GetRecipeDto GetRecipe(Guid id);
        public Guid CreateRecipe(CreateRecipeDto createDto);
        public void UpdateRecipe(Guid id, UpdateRecipeDto updateDto);
        public void LikeRecipe(Guid id);
        public void DeleteRecipe(Guid id);
    }
}
