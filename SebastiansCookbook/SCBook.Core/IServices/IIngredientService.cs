using System;
using System.Collections.Generic;
using SCBook.Core.DTOs.Ingredient;

namespace SCBook.Core.IServices
{
    public interface IIngredientService
    {
        public GetIngredientDto GetIngredient(Guid id);
        public IEnumerable<GetIngredientDto> GetIngredientList();
        public void UpdateIngredient(Guid id, UpdateIngredientDto updateDto);
        public Guid CreateIngredient(CreateIngredientDto createDto);
        public void DeleteIngredient(Guid id);
    }
}
