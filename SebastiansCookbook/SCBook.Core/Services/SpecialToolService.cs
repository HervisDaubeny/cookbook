using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using SCBook.Core.DTOs.SpecialTool;
using SCBook.Core.Mappers;
using SCBook.Core.Extensions;
using SCBook.Dal.Catalogs;
using SCBook.Core.IServices;

namespace SCBook.Core.Services
{
    public class SpecialToolService : ISpecialToolService
    {
        private readonly Dal.SCBookContext _scbContext;
        private DbSet<SpecialTool> SpecialToolDbData => _scbContext.SpecialTools;

        public SpecialToolService(Dal.SCBookContext context)
        {
            _scbContext = context;
        }

        public Guid CreateSpecialTool(CreateSpecialToolDto createDto)
        {
            Guid id = Guid.NewGuid();
            var newSpecialTool = createDto.ToCatalog(id);
            SpecialToolDbData.Add(newSpecialTool);
            WriteChanges();
            return (id);
        }

        public void DeleteSpecialTool(Guid id)
        {
            var entity = SpecialToolDbData.FindById(id);
            SpecialToolDbData.Remove(entity);
            WriteChanges();
        }

        public GetSpecialToolDto GetSpecialTool(Guid id)
        {
            return SpecialToolDbData.FindById(id).ToDto();
        }

        public IEnumerable<GetSpecialToolDto> GetSpecialToolList()
        {
            return SpecialToolDbData.ToDtos();
        }

        public void UpdateSpecialTool(Guid id, UpdateSpecialToolDto updateDto)
        {
            var data = SpecialToolDbData.FindById(id);
            data.UpdateWith(updateDto);
            WriteChanges();
        }

        private void WriteChanges()
        {
            _scbContext.SaveChanges();
        }
    }
}
