﻿using Microsoft.EntityFrameworkCore;
using SCBook.Core.DTOs.Recipe;
using SCBook.Core.Extensions;
using SCBook.Core.IServices;
using SCBook.Core.Mappers;
using SCBook.Dal.Entities;
using System;
using System.Linq;

namespace SCBook.Core.Services
{
    public class RecipeService : IRecipeService
    {
        private readonly Dal.SCBookContext _scbContext;
        private DbSet<Recipe> RecipeDbData => _scbContext.Recipes;

        public RecipeService(Dal.SCBookContext scbContext)
        {
            _scbContext = scbContext;
        }

        public Guid CreateRecipe(CreateRecipeDto createDto)
        {
            var id = Guid.NewGuid();
            var recipe = createDto.ToEntity(id);
            RecipeDbData.Add(recipe);
            WriteChanges();
            return id;
        }

        public void DeleteRecipe(Guid id)
        {
            var entity = RecipeDbData.FindById(id);
            RecipeDbData.Remove(entity);
            WriteChanges();
        }

        public GetRecipeDto GetRecipe(Guid id)
        {
            var recipe = RecipeDbData
                .Include(r => r.Author)
                .Include(r => r.Difficulty)
                .Include(r => r.RecipeCategories)
                .ThenInclude(rc => rc.Category)
                .Include(r => r.RecipeIngredients)
                .ThenInclude(ri => ri.Ingredient)
                .Include(r => r.RecipeSpecialTools)
                .ThenInclude(rst => rst.SpecialTool)
                .Include(r => r.Reviews)
                .FindById(id)
                .ToDto();

            return recipe;
        }

        public GetRecipeListDto GetRecipeList(FiltrationAndPaginationDto filterPageDto)
        {
            int filtered = 0;
            int total = 0;

            var query = RecipeDbData
                .Include(r => r.Author)
                .Include(r => r.Difficulty)
                .Include(r => r.RecipeCategories)
                .ThenInclude(rc => rc.Category)
                .Include(r => r.RecipeIngredients)
                .ThenInclude(ri => ri.Ingredient)
                .Include(r => r.RecipeSpecialTools)
                .ThenInclude(rst => rst.SpecialTool)
                .Include(r => r.Reviews);
            total = query.Count();

            var filteredQuery = query.FilterBy(filterPageDto.FilterType, filterPageDto.FilterValue);
            filtered = filteredQuery.Count();

            return filteredQuery
                .Skip(filterPageDto.Offset)
                .Take(filterPageDto.Take)
                .ToDtos()
                .ToListDto(filtered, total);
        }

        public void UpdateRecipe(Guid id, UpdateRecipeDto updateDto)
        {
            using var transaction = _scbContext.Database.BeginTransaction();

            var entity = RecipeDbData
                .Include(r => r.RecipeCategories)
                .Include(r => r.RecipeIngredients)
                .Include(r => r.RecipeSpecialTools)
                .FindById(id);
            entity.UpdateWith(updateDto);
            WriteChanges();
            transaction.Commit();
        }

        public void LikeRecipe(Guid id)
        {
            using var transaction = _scbContext.Database.BeginTransaction();

            var entity = RecipeDbData.FindById(id);
            entity.LikeCount++;
            WriteChanges();
            transaction.Commit();
        }

        private void WriteChanges()
        {
            _scbContext.SaveChanges();
        }
    }
}
