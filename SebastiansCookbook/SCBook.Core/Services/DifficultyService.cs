using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using SCBook.Core.DTOs.Difficulty;
using SCBook.Core.Mappers;
using SCBook.Core.Extensions;
using SCBook.Dal.Catalogs;
using SCBook.Core.IServices;

namespace SCBook.Core.Services
{
    public class DifficultyService : IDifficultyService
    {
        private readonly Dal.SCBookContext _scbContext;
        private DbSet<Difficulty> DifficultyDbData => _scbContext.Difficulties;

        public DifficultyService(Dal.SCBookContext context)
        {
            _scbContext = context;
        }

        public Guid CreateDifficulty(CreateDifficultyDto createDto)
        {
            Guid id = Guid.NewGuid();
            var newDifficulty = createDto.ToCatalog(id);
            DifficultyDbData.Add(newDifficulty);
            WriteChanges();
            return (id);
        }

        public void DeleteDifficulty(Guid id)
        {
            var entity = DifficultyDbData.FindById(id);
            DifficultyDbData.Remove(entity);
            WriteChanges();
        }

        public GetDifficultyDto GetDifficulty(Guid id)
        {
            return DifficultyDbData.FindById(id).ToDto();
        }

        public IEnumerable<GetDifficultyDto> GetDifficultyList()
        {
            return DifficultyDbData.ToDtos();
        }

        public void UpdateDifficulty(Guid id, UpdateDifficultyDto updateDto)
        {
            var data = DifficultyDbData.FindById(id);
            data.UpdateWith(updateDto);
            WriteChanges();
        }

        private void WriteChanges()
        {
            _scbContext.SaveChanges();
        }
    }
}
