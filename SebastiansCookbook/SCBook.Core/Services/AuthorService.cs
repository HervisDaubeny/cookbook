using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using SCBook.Core.DTOs.Author;
using SCBook.Core.Mappers;
using SCBook.Core.Extensions;
using SCBook.Dal.Catalogs;
using SCBook.Core.IServices;

namespace SCBook.Core.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly Dal.SCBookContext _scbContext;
        private DbSet<Author> AuthorDbData => _scbContext.Authors;

        public AuthorService(Dal.SCBookContext context)
        {
            _scbContext = context;
        }

        public Guid CreateAuthor(CreateAuthorDto createDto)
        {
            Guid id = Guid.NewGuid();
            var newAuthor = createDto.ToCatalog(id);
            AuthorDbData.Add(newAuthor);
            WriteChanges();
            return (id);
        }

        public void DeleteAuthor(Guid id)
        {
            var entity = AuthorDbData.FindById(id);
            AuthorDbData.Remove(entity);
            WriteChanges();
        }

        public GetAuthorDto GetAuthor(Guid id)
        {
            return AuthorDbData.FindById(id).ToDto();
        }

        public IEnumerable<GetAuthorDto> GetAuthorList()
        {
            return AuthorDbData.ToDtos();
        }

        public void UpdateAuthor(Guid id, UpdateAuthorDto updateDto)
        {
            var data = AuthorDbData.FindById(id);
            data.UpdateWith(updateDto);
            WriteChanges();
        }

        private void WriteChanges()
        {
            _scbContext.SaveChanges();
        }
    }
}
