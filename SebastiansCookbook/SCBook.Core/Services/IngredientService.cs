using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using SCBook.Core.DTOs.Ingredient;
using SCBook.Core.Mappers;
using SCBook.Core.Extensions;
using SCBook.Dal.Catalogs;
using SCBook.Core.IServices;

namespace SCBook.Core.Services
{
    public class IngredientService : IIngredientService
    {
        private readonly Dal.SCBookContext _scbContext;
        private DbSet<Ingredient> IngredientDbData => _scbContext.Ingredients;

        public IngredientService(Dal.SCBookContext context)
        {
            _scbContext = context;
        }

        public Guid CreateIngredient(CreateIngredientDto createDto)
        {
            Guid id = Guid.NewGuid();
            var newIngredient = createDto.ToCatalog(id);
            IngredientDbData.Add(newIngredient);
            WriteChanges();
            return (id);
        }

        public void DeleteIngredient(Guid id)
        {
            var entity = IngredientDbData.FindById(id);
            IngredientDbData.Remove(entity);
            WriteChanges();
        }

        public GetIngredientDto GetIngredient(Guid id)
        {
            return IngredientDbData.FindById(id).ToDto();
        }

        public IEnumerable<GetIngredientDto> GetIngredientList()
        {
            return IngredientDbData.ToDtos();
        }

        public void UpdateIngredient(Guid id, UpdateIngredientDto updateDto)
        {
            var data = IngredientDbData.FindById(id);
            data.UpdateWith(updateDto);
            WriteChanges();
        }

        private void WriteChanges()
        {
            _scbContext.SaveChanges();
        }
    }
}
