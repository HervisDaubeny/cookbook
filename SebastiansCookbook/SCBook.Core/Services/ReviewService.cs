﻿using Microsoft.EntityFrameworkCore;
using SCBook.Core.DTOs.Review;
using SCBook.Core.Extensions;
using SCBook.Core.IServices;
using SCBook.Core.Mappers;
using SCBook.Dal.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SCBook.Core.Services
{
    public class ReviewService : IReviewService
    {
        private readonly Dal.SCBookContext _scbContext;
        private DbSet<Review> ReviewDbData => _scbContext.Reviews;

        public ReviewService(Dal.SCBookContext scbContext)
        {
            _scbContext = scbContext;
        }

        public Guid CreateReview(CreateReviewDto createDto)
        {
            var id = Guid.NewGuid();
            var review = createDto.ToEntity(id);
            ReviewDbData.Add(review);
            WriteChanges();
            return id;
        }

        public IEnumerable<GetReviewDetailDto> GetReviewList(Guid recipeId)
        {
            var reviews = ReviewDbData
                .Include(r => r.Recipe)
                .Where(r => r.RecipeId == recipeId)
                .ToArray()
                .ToDtos();

            return reviews;
        }

        public GetReviewDetailDto GetReview(Guid reviewId)
        {
            var review = ReviewDbData
                .Include(r => r.Recipe)
                .FindById(reviewId)
                .ToDto();

            return review;
        }

        public void DeleteReview(Guid id)
        {
            var review = ReviewDbData.FindById(id);
            ReviewDbData.Remove(review);
            WriteChanges();
        }

        private void WriteChanges()
        {
            _scbContext.SaveChanges();
        }
    }
}
