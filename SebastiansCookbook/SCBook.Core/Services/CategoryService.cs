using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using SCBook.Core.DTOs.Category;
using SCBook.Core.Mappers;
using SCBook.Core.Extensions;
using SCBook.Dal.Catalogs;
using SCBook.Core.IServices;

namespace SCBook.Core.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly Dal.SCBookContext _scbContext;
        private DbSet<Category> CategoryDbData => _scbContext.Categories;

        public CategoryService(Dal.SCBookContext context)
        {
            _scbContext = context;
        }

        public Guid CreateCategory(CreateCategoryDto createDto)
        {
            Guid id = Guid.NewGuid();
            var newCategory = createDto.ToCatalog(id);
            CategoryDbData.Add(newCategory);
            WriteChanges();
            return (id);
        }

        public void DeleteCategory(Guid id)
        {
            var entity = CategoryDbData.FindById(id);
            CategoryDbData.Remove(entity);
            WriteChanges();
        }

        public GetCategoryDto GetCategory(Guid id)
        {
            return CategoryDbData.FindById(id).ToDto();
        }

        public IEnumerable<GetCategoryDto> GetCategoryList()
        {
            return CategoryDbData.ToDtos();
        }

        public void UpdateCategory(Guid id, UpdateCategoryDto updateDto)
        {
            var data = CategoryDbData.FindById(id);
            data.UpdateWith(updateDto);
            WriteChanges();
        }

        private void WriteChanges()
        {
            _scbContext.SaveChanges();
        }
    }
}
