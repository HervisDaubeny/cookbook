﻿using SCBook.Core.Enums;
using SCBook.Dal.Entities;
using System;
using System.Linq;

namespace SCBook.Core.Extensions
{
    public static class RecipeFilterExtensions
    {
        /// <summary>
        /// Return the entities that satisfy the filter provided.
        /// </summary>
        /// <param name="query">Querry with all relations included.</param>
        /// <param name="filterType">Enum deciding the type of filtration</param>
        /// <param name="filterValue">Value to use for filtration</param>
        /// <returns>Array of filtered Recipes</returns>
        public static IQueryable<Recipe> FilterBy(
            this IQueryable<Recipe> query, 
            RecipeFilter filterType,
            string filterValue
            )
        {
            var recipes = filterType switch
            {
                RecipeFilter.None => query,
                RecipeFilter.Title => query.Where(r => r.Title.Contains(filterValue)),
                RecipeFilter.Difficulty => query.Where(r => r.Difficulty.Value.Contains(filterValue)),
                RecipeFilter.Ingredient => query.Where(r => r.RecipeIngredients.Any(ri => ri.Ingredient.Value.Contains(filterValue))),
                RecipeFilter.TimeToPrepare => query.Where(r => r.PrepTime <= int.Parse(filterValue)),
                _ => throw new NotImplementedException(),
            };

            return recipes;
        }
    }
}
