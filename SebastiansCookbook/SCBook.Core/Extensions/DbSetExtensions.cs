﻿using Microsoft.EntityFrameworkCore;
using SCBook.Core.Exceptions;
using SCBook.Dal.Entities;
using System;
using System.Linq;

namespace SCBook.Core.Extensions
{
    public static class DbSetExtensions
    {
        /// <summary>
        /// Return the entity with the provided Id. If it does not exist, throw the
        /// KeyDoesNotExistException.
        /// </summary>
        /// <typeparam name="TEntity">Type of the entity</typeparam>
        /// <param name="dbSet">The DbSet to search for the id</param>
        /// <param name="id">The id of the entity</param>
        /// <returns>The entity</returns>
        public static TEntity FindById<TEntity>(this DbSet<TEntity> dbSet, Guid id) where TEntity : class
        {
            var entity = dbSet.Find(id);
            entity = entity ?? throw new KeyDoesNotExistException($"Key '{id}' does not exist on entity '{nameof(TEntity)}'");
            return entity;
        }

        /// <summary>
        /// Return the entity with the provided Id from the included query.
        /// If it does not exist, throw the KeyDoesNotExistException.
        /// </summary>
        /// <typeparam name="TEntity">Type of the entity</typeparam>
        /// <typeparam name="TQuerry">Type of the query</typeparam>
        /// <param name="data">The query to search for the id</param>
        /// <param name="id">The id of the entity</param>
        /// <returns>The entity</returns>
        public static TEntity FindById<TEntity, TQuerry>(
            this Microsoft.EntityFrameworkCore.Query.IIncludableQueryable<TEntity, TQuerry>
            data, Guid id) where TEntity : class, IEntity
        {
            var entity = data.SingleOrDefault(x => x.Id == id);
            entity = entity ?? throw new KeyDoesNotExistException($"Key '{id}' does not exist on enitity '{nameof(TEntity)}'");
            return entity;
        }

        /// <summary>
        /// Return the number of existing entities.
        /// </summary>
        /// <typeparam name="TEntity">Type of the entity</typeparam>
        /// <typeparam name="TQuerry">Type of the query</typeparam>
        /// <param name="data">The query to search for the id</param>
        /// <returns>Count of entities</returns>
        public static int Count<TEntity, TQuerry>(
            this Microsoft.EntityFrameworkCore.Query.IIncludableQueryable<TEntity, TQuerry>
            data) where TEntity : class, IEntity
        {
            return data.Select(d => d.Id).Count();
        }
    }
}
