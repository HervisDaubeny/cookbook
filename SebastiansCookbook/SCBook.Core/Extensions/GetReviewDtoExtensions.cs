﻿using SCBook.Core.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace SCBook.Core.Extensions
{
    public static class GetReviewExtension
    {
        /// <summary>
        /// Create GetReviewDto from given RecipeReview
        /// </summary>
        /// <param name="review">RecipeReview to use</param>
        /// <returns>GetReviewDto created from <paramref name="review"/></returns>
        public static GetReviewDto ToGetReviewDto(this Dal.Entities.Review review)
        {
            return new GetReviewDto
            {
                Id = review.Id,
                Value = review.Value,
                ReviewersName = review.ReviewersName,
                TimeOfReview = review.TimeOfReview,
                PublishNameConsent = review.PublishNameConsent
            };
        }

        /// <summary>
        /// Create GetReviewDto[] from collection of RecipeReview
        /// </summary>
        /// <param name="recipeReviews">RecipeReview collection to use</param>
        /// <returns>GetReviewDto[] created from <paramref name="recipeReviews"/></returns>
        public static GetReviewDto[] ToGetReviewDtos(this IEnumerable<Dal.Entities.Review> recipeReviews)
        {
            return recipeReviews.Select(r => r.ToGetReviewDto()).ToArray();
        }
    }
}
