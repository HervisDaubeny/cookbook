﻿using SCBook.Core.DTOs;
using SCBook.Dal.Catalogs;
using SCBook.Dal.JoiningEntities;
using System.Collections.Generic;
using System.Linq;

namespace SCBook.Core.Extensions
{
    public static class GetCatalogPairExtensions
    {
        /// <summary>
        /// Create CatalogPairDto from given ICatalogEntity.
        /// </summary>
        /// <param name="catalogEntity">ICatalogEntity to use</param>
        /// <returns>CatalogPairDto created from <paramref name="catalogEntity"/></returns>
        public static Core.DTOs.GetCatalogPairDto ToCatalogPairDto(this ICatalogEntity catalogEntity)
        {
            return new Core.DTOs.GetCatalogPairDto { Id = catalogEntity.Id, Value = catalogEntity.Value };
        }

        /// <summary>
        /// Create CatalogPairDto[] from given collection of RecipeCategory
        /// </summary>
        /// <param name="recipeCategories">RecipeCategory collection to use</param>
        /// <returns>CatalogPairDto[] created from <paramref name="recipeCategories"/></returns>
        public static Core.DTOs.GetCatalogPairDto[] ToCategoryCatalogPairDtos (this IEnumerable<RecipeCategory> recipeCategories)
        {
            return recipeCategories.Select
                ( rc => new GetCatalogPairDto { Id = rc.CategoryId, Value = rc.Category.Value } ).ToArray();
        }

        /// <summary>
        /// Create CatalogPairDto[] from given collection of RecipeIngredient
        /// </summary>
        /// <param name="recipeIngredients">RecipeIngredient collection to use</param>
        /// <returns>CatalogPairDto[] created from <paramref name="recipeIngredients"/></returns>
        public static Core.DTOs.GetCatalogPairDto[] ToIngredientCatalogPairDtos(this IEnumerable<RecipeIngredient> recipeIngredients)
        {
            return recipeIngredients.Select
                (rc => new GetCatalogPairDto { Id = rc.IngredientId, Value = rc.Ingredient.Value }).ToArray();
        }

        /// <summary>
        /// Create CatalogPairDto[] from given collection of RecipeSpecialTool
        /// </summary>
        /// <param name="recipeSpecialTools">RecipeSpecialTool collection to use</param>
        /// <returns>CatalogPairDto[] created from <paramref name="recipeSpecialTools"/></returns>
        public static Core.DTOs.GetCatalogPairDto[] ToSpecialToolCatalogPairDtos(this IEnumerable<RecipeSpecialTool> recipeSpecialTools)
        {
            return recipeSpecialTools.Select
                (rc => new GetCatalogPairDto { Id = rc.SpecialToolId, Value = rc.SpecialTool.Value }).ToArray();
        }
    }
}
