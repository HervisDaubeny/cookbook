﻿using System;
using System.Collections.Generic;

namespace SCBook.Core.Extensions
{
    public static class RecipeUpdatingNMRelationsExtensions
    {
        /// <summary>
        /// Remove relations given in <paramref name="relations"/> from the collection
        /// of JoiningEntities given in <paramref name="collection"/>.
        /// </summary>
        /// <typeparam name="TJoiningEntity">Type of the JoiningEntity</typeparam>
        /// <param name="collection">Collection of JoiningEntities</param>
        /// <param name="relations">Collection of JoiningEntities to remove from <paramref name="collection"/></param>
        /// <returns><paramref name="collection"/> without <paramref name="relations"/></returns>
        public static ICollection<TJoiningEntity> RemoveJoiningRelations<TJoiningEntity>(this ICollection<TJoiningEntity> collection, IEnumerable<TJoiningEntity> relations)
        {
            foreach (var relation in relations)
            {
                collection.Remove(relation);
            }

            return collection;
        }

        /// <summary>
        /// Add relations given in <paramref name="relations"/> to the collection
        /// of JoiningEntities given in <paramref name="collection"/>.
        /// </summary>
        /// <typeparam name="TJoiningEntity">Type of the JoiningEntity</typeparam>
        /// <param name="collection">Collection of JoiningEntities</param>
        /// <param name="relations">Collection of JoiningEntities to add to <paramref name="collection"/></param>
        /// <returns><paramref name="collection"/> extended of <paramref name="relations"/></returns>
        public static ICollection<TJoiningEntity> AddJoiningRelations<TJoiningEntity>(this ICollection<TJoiningEntity> collection, IEnumerable<TJoiningEntity> relations)
        {
            foreach (var relation in relations)
            {
                collection.Add(relation);
            }

            return collection;
        }

        /// <summary>
        /// Update JoiningRelations of the <paramref name="entity"/>s property we get using <paramref name="propertySelector"/>.
        /// Create <typeparamref name="TJoiningEntity"/> from <paramref name="addIds"/> and <paramref name="entity"/>s Id and Add them to <typeparamref name="TJoiningEntity"/> collection.
        /// Delete <paramref name="deleteRelations"/> from <typeparamref name="TJoiningEntity"/> collection.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TJoiningEntity"></typeparam>
        /// <param name="entity"></param>
        /// <param name="propertySelector"></param>
        /// <param name="addIds"></param>
        /// <param name="createRelation"></param>
        /// <param name="deleteRelations"></param>
        /// <returns>Entity with <typeparamref name="TJoiningEntity"/> updated.</returns>
        public static TEntity UpdateJoiningRelations<TEntity, TJoiningEntity>(
            this TEntity entity,
            Func<TEntity, ICollection<TJoiningEntity>> propertySelector,
            IEnumerable<Guid> addIds,
            Func<IEnumerable<Guid>, IEnumerable<TJoiningEntity>> createRelation,
            IEnumerable<TJoiningEntity> deleteRelations)
        {
            propertySelector(entity).RemoveJoiningRelations(deleteRelations);
            propertySelector(entity).AddJoiningRelations(createRelation(addIds));

            return entity;
        }
    }
}
