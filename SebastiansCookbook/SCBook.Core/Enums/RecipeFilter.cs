﻿using System.Runtime.Serialization;

namespace SCBook.Core.Enums
{
    public enum RecipeFilter
    {
        [EnumMember(Value = "None")]
        None,
        [EnumMember(Value = "Title")]
        Title,
        [EnumMember(Value = "Difficulty")]
        Difficulty,
        [EnumMember(Value = "Ingredient")]
        Ingredient,
        [EnumMember(Value = "TimeToPrepare")]
        TimeToPrepare,        
    }
}
