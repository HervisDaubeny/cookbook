﻿using System;

namespace SCBook.Core.DTOs.Recipe
{
    /// <summary>
    /// Data transfer object for Recipe entity create request
    /// </summary>
    public class CreateRecipeDto
    {
        /// <summary>
        /// Title of Recipe entity
        /// </summary>
        public string Title { get; set; } = default;

        /// <summary>
        /// PrepTime of Recipe entity
        /// </summary>
        public int PrepTime { get; set; }

        /// <summary>
        /// Directions of Recipe entity
        /// </summary>
        public string Directions { get; set; } = default;

        /// <summary>
        /// Link to photo of Recipe entity
        /// </summary>
        public string PhotoLink { get; set; } = default;

        /// <summary>
        /// Id of Author catalog entry that authored Recipe
        /// </summary>
        public Guid AuthorId { get; set; } = default;

        /// <summary>
        /// Id of Difficulty catalog entry that Recipe has
        /// </summary>
        public Guid DifficultyId { get; set; } = default;

        /// <summary>
        /// Ids of Category catalog entries that Recipe belongs to
        /// </summary>
        public Guid[] CategoryIds { get; set; } = default;

        /// <summary>
        /// Ids of Ingredient catalog entries that Recipe requires
        /// </summary>
        public Guid[] IngredientIds { get; set; } = default;

        /// <summary>
        /// Ids of SpecialTool catalog entries that Recipe requires
        /// </summary>
        public Guid[] SpecialToolIds { get; set; } = default;
    }
}
