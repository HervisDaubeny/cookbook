﻿using System;

namespace SCBook.Core.DTOs.Recipe
{
    /// <summary>
    /// Data transfer object for Recipe entity get request
    /// </summary>
    public class GetRecipeDto
    {
        /// <summary>
        /// UUID of Recipe entity
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Title of Recipe entity
        /// </summary>
        public string Title { get; set; } = default;

        /// <summary>
        /// PrepTime of Recipe entity
        /// </summary>
        public int PrepTime { get; set; }

        /// <summary>
        /// Directions of Recipe entity
        /// </summary>
        public string Directions { get; set; } = default;

        /// <summary>
        /// LikeCount of Recipe entity
        /// </summary>
        public int LikeCount { get; set; }

        /// <summary>
        /// PhotoLink of Recipe entity
        /// </summary>
        public string PhotoLink { get; set; }

        /// <summary>
        /// Author catalog entry that created Recipe
        /// </summary>
        public GetCatalogPairDto Author { get; set; } = default;

        /// <summary>
        /// Difficulty catalog entry that Recipe has
        /// </summary>
        public GetCatalogPairDto Difficulty { get; set; } = default;

        /// <summary>
        /// Category catalog entries that Recipe belongs to
        /// </summary>
        public GetCatalogPairDto[] Categories { get; set; } = default;

        /// <summary>
        /// Ingredient catalog entries that Recipe requires
        /// </summary>
        public GetCatalogPairDto[] Ingredients { get; set; } = default;

        /// <summary>
        /// SpecialTool catalog entries that Recipe requires
        /// </summary>
        public GetCatalogPairDto[] SpecialTools { get; set; } = default;

        /// <summary>
        /// Review entities that belong to Recipe
        /// </summary>
        public GetReviewDto[] Reviews { get; set; } = default;
    }
}
