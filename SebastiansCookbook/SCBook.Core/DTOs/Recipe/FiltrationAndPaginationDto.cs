﻿using SCBook.Core.Enums;

namespace SCBook.Core.DTOs.Recipe
{
    public class FiltrationAndPaginationDto
    {
        /// <summary>
        /// Filter type to use. Defaults to None
        /// </summary>
        public RecipeFilter FilterType { get; set; }

        /// <summary>
        /// Value to filter by
        /// </summary>
        public string FilterValue { get; set; }

        /// <summary>
        /// Number of results from query to skip
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Required number of results after the Offset
        /// </summary>
        public int Take { get; set; }
    }
}
