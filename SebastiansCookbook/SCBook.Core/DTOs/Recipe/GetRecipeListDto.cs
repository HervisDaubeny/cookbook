﻿namespace SCBook.Core.DTOs.Recipe
{
    /// <summary>
    /// DTO for List of Recipes
    /// </summary>
    public class GetRecipeListDto
    {
        /// <summary>
        /// Number of Recipe entities that passed the filtration
        /// </summary>
        public int Filtered { get; set; }

        /// <summary>
        /// Number of Recipe entities in the database
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// Collection of GetRecipeDto
        /// </summary>
        public GetRecipeDto[] Collection { get; set; }
    }
}
