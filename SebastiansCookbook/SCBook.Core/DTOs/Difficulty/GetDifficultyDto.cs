using System;

// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Difficulty
{
    /// <summary>
    /// Output message for Difficulty catalog detail get request
    /// </summary>
    public class GetDifficultyDto
    {
        /// <summary>
        /// UUID of catalog Difficulty
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Value of catalog Difficulty
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Difficulty catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
