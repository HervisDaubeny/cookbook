// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Difficulty
{
    /// <summary>
    /// Data transfer object for Difficulty catalog creating request
    /// </summary>
    public class CreateDifficultyDto
    {
        /// <summary>
        /// Value of catalog Difficulty
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
