// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Difficulty
{
    /// <summary>
    /// Data transfer object for Difficulty catalog updating request
    /// </summary>
    public class UpdateDifficultyDto
    {
        /// <summary>
        /// Value of catalog Difficulty
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Difficulty catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
