﻿using System;

namespace SCBook.Core.DTOs
{
    /// <summary>
    /// Data transfer object for Review entity get request
    /// </summary>
    public class GetReviewDto
    {
        /// <summary>
        /// UUID of Review entity
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Value of Review entity
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Name of author of Review entity
        /// </summary>
        public string ReviewersName { get; set; } = default!;

        /// <summary>
        /// Timestamp of Review entity creation
        /// </summary>
        public DateTimeOffset TimeOfReview { get; set; }

        /// <summary>
        /// Consent to publish authors name
        /// </summary>
        public bool PublishNameConsent { get; set; } = default!;
    }
}
