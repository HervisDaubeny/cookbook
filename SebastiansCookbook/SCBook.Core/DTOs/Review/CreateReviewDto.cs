﻿using System;

namespace SCBook.Core.DTOs.Review
{
    /// <summary>
    /// Data transfer object for Review entity create request
    /// </summary>
    public class CreateReviewDto
    {
        /// <summary>
        /// Value of Review entity
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Name of author of Review entity
        /// </summary>
        public string ReviewersName { get; set; } = default!;

        /// <summary>
        /// Timestamp of Review entity creation
        /// </summary>
        public DateTimeOffset TimeOfReview { get; set; }

        /// <summary>
        /// Consent to publish authors name
        /// </summary>
        public bool PublishNameConsent { get; set; } = default!;

        /// <summary>
        /// Id of Recipe entity that Review is about
        /// </summary>
        public Guid RecipeId { get; set; } = default;
    }
}
