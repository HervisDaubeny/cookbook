// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Ingredient
{
    /// <summary>
    /// Data transfer object for Ingredient catalog creating request
    /// </summary>
    public class CreateIngredientDto
    {
        /// <summary>
        /// Value of catalog Ingredient
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
