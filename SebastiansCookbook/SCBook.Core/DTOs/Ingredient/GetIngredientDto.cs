using System;

// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Ingredient
{
    /// <summary>
    /// Output message for Ingredient catalog detail get request
    /// </summary>
    public class GetIngredientDto
    {
        /// <summary>
        /// UUID of catalog Ingredient
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Value of catalog Ingredient
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Ingredient catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
