// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Ingredient
{
    /// <summary>
    /// Data transfer object for Ingredient catalog updating request
    /// </summary>
    public class UpdateIngredientDto
    {
        /// <summary>
        /// Value of catalog Ingredient
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Ingredient catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
