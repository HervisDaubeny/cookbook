using System;

// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Category
{
    /// <summary>
    /// Output message for Category catalog detail get request
    /// </summary>
    public class GetCategoryDto
    {
        /// <summary>
        /// UUID of catalog Category
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Value of catalog Category
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Category catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
