// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Category
{
    /// <summary>
    /// Data transfer object for Category catalog updating request
    /// </summary>
    public class UpdateCategoryDto
    {
        /// <summary>
        /// Value of catalog Category
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Category catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
