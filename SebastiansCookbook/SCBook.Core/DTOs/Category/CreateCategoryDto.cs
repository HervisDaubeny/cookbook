// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Category
{
    /// <summary>
    /// Data transfer object for Category catalog creating request
    /// </summary>
    public class CreateCategoryDto
    {
        /// <summary>
        /// Value of catalog Category
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
