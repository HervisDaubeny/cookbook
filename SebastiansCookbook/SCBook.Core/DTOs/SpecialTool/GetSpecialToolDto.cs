using System;

// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.SpecialTool
{
    /// <summary>
    /// Output message for SpecialTool catalog detail get request
    /// </summary>
    public class GetSpecialToolDto
    {
        /// <summary>
        /// UUID of catalog SpecialTool
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Value of catalog SpecialTool
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of SpecialTool catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
