// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.SpecialTool
{
    /// <summary>
    /// Data transfer object for SpecialTool catalog updating request
    /// </summary>
    public class UpdateSpecialToolDto
    {
        /// <summary>
        /// Value of catalog SpecialTool
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of SpecialTool catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
