// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.SpecialTool
{
    /// <summary>
    /// Data transfer object for SpecialTool catalog creating request
    /// </summary>
    public class CreateSpecialToolDto
    {
        /// <summary>
        /// Value of catalog SpecialTool
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
