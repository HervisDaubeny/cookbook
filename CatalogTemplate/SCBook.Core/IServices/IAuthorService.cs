using System;
using System.Collections.Generic;
using SCBook.Core.DTOs.Author;

namespace SCBook.Core.IServices
{
    public interface IAuthorService
    {
        public GetAuthorDto GetAuthor(Guid id);
        public IEnumerable<GetAuthorDto> GetAuthorList();
        public void UpdateAuthor(Guid id, UpdateAuthorDto updateDto);
        public Guid CreateAuthor(CreateAuthorDto createDto);
        public void DeleteAuthor(Guid id);
    }
}
