using System;

// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Author
{
    /// <summary>
    /// Data transfer object for Author catalog updating request
    /// </summary>
    public class UpdateAuthorDto
    {
        /// <summary>
        /// Value of catalog Author
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Author catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
