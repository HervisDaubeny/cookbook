using System;

// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Author
{
    /// <summary>
    /// Output message for Author catalog detail get request
    /// </summary>
    public class GetAuthorDto
    {
        /// <summary>
        /// UUID of catalog Author
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Value of catalog Author
        /// </summary>
        public string Value { get; set; } = default!;

        /// <summary>
        /// Active flag of Author catalog value
        /// </summary>
        public bool Active { get; set; } = default!;
    }
}
