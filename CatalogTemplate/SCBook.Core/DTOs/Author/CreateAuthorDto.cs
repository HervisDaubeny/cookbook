using System;

// ReSharper disable once CheckNamespace
namespace SCBook.Core.DTOs.Author
{
    /// <summary>
    /// Data transfer object for Author catalog creating request
    /// </summary>
    public class CreateAuthorDto
    {
        /// <summary>
        /// Value of catalog Author
        /// </summary>
        public string Value { get; set; } = default!;
    }
}
