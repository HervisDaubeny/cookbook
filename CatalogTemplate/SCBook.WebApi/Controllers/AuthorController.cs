using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using SCBook.WebApi.Messages;
using SCBook.WebApi.Mappers;
using Microsoft.AspNetCore.Http;
using SCBook.Core.IServices;

namespace SCBook.WebApi.Controllers
{
    /// <summary>
    /// Controller defining Author catalog endpoints.
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class AuthorController : ControllerBase
    {
        private readonly ILogger<AuthorController> _logger;
        private readonly IAuthorService _authorService;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="authorService"></param>
        public AuthorController(ILogger<AuthorController> logger, IAuthorService authorService)
        {
            _logger = logger;
            _authorService = authorService;
        }

        /// <summary>
        /// Get list of existing Author catalog values.
        /// </summary>
        /// <returns>Https response</returns>
        /// <response code="200">Author catalog entry list successfully found</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetAuthorResponse))]
        public IActionResult GetAuthorList()
        {
            var authorList = _authorService.GetAuthorList().ToMessage();
            return Ok(authorList);
        }

        /// <summary>
        /// Get value of Author catalog entry specified by id.
        /// </summary>
        /// <param name="id">Author identification</param>
        /// <returns>Http response</returns>
        /// <response code="200">Author successfully found</response>
        /// <response code="404">Errors occurred during lookup process</response>
        [HttpGet("{id:Guid}", Name = nameof(GetAuthorItem))]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetAuthorResponseItem))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult GetAuthorItem([FromRoute] Guid id)
        {
            var author = _authorService.GetAuthor(id).ToMessage();
            return Ok(author);
        }

        /// <summary>
        /// Create new Author catalog entry.
        /// </summary>
        /// <param name="requestBody">DTO for Author catalog entry creation</param>
        /// <returns>Http response</returns>
        /// <response code="201">Author catalog entry value successfully created</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(GeneralCreatedIdResponse))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        public IActionResult CreateAuthorItem([FromBody] CreateAuthorRequest requestBody)
        {
            Guid createdId = _authorService.CreateAuthor(requestBody.ToDto());
            var url = Url.Link(nameof(GetAuthorItem), new { id = createdId });
            return Created(url, new GeneralCreatedIdResponse() { Id = createdId });
        }

        /// <summary>
        /// Update value of Author catalog entry.
        /// </summary>
        /// <param name="id">Author catalog entry identification</param>
        /// <param name="requestBody">DTO for Author catalog entry update</param>
        /// <returns>Http response</returns>
        /// <response code="200">Author catalog entry value successfully updated</response>
        /// <response code="400"><paramref name="requestBody"/> is invalid</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpPut("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ValidationProblemDetails))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult UpdateAuthorItem([FromRoute] Guid id, [FromBody] UpdateAuthorRequest requestBody)
        {
            _authorService.UpdateAuthor(id, requestBody.ToDto());
            return Ok();
        }

        /// <summary>
        /// Delete specified Author catalog entry.
        /// </summary>
        /// <param name="id">Author catalog entry specification</param>
        /// <returns>Http response</returns>
        /// <response code="204">Author catalog entry value successfully deleted</response>
        /// <response code="404"><paramref name="id"/> not found</response>
        [HttpDelete("{id:Guid}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ValidationProblemDetails))]
        public IActionResult DeleteAuthorItem([FromRoute] Guid id)
        {
            _authorService.DeleteAuthor(id);
            return NoContent();
        }
    }
}
