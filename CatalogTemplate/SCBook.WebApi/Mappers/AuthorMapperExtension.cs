using System.Collections.Generic;
using System.Linq;
using SCBook.Core.DTOs.Author;
using SCBook.WebApi.Messages;

namespace SCBook.WebApi.Mappers
{
    /// <summary>
    /// Extension methods for AuthorMappers
    /// </summary>
    public static class AuthorMapperExtension
    {
        /// <summary>
        /// Create UpdateAuthorDto from UpdateAuthorRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static UpdateAuthorDto ToDto(this UpdateAuthorRequest request)
        {
            return new UpdateAuthorDto() { Value = request.Value, Active = request.Active!.Value };
        }

        /// <summary>
        /// Create CreateAuthorDto from CreateAuthorRequest message
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static CreateAuthorDto ToDto(this CreateAuthorRequest request)
        {
            return new CreateAuthorDto() { Value = request.Value };
        }

        /// <summary>
        /// Create GetAuthorResponseItem message from GetAuthorDto
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static GetAuthorResponseItem ToMessage(this GetAuthorDto dto)
        {
            return new GetAuthorResponseItem() { Id = dto.Id, Value = dto.Value, Active = dto.Active };
        }

        /// <summary>
        /// Create GetAuthorResponse message from collection of GetAuthorDto
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static GetAuthorResponse ToMessage(this IEnumerable<GetAuthorDto> dtos)
        {
            var message = new GetAuthorResponse();
            message.Collection = dtos.Select(d => d.ToMessage()).ToArray();
            return (message);
        }
    }
}
