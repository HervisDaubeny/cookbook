using SCBook.WebApi.Messages;
using FluentValidation;

namespace SCBook.WebApi.Validators.Author
{
    /// <summary>
    /// 
    /// </summary>
    public class CreateAuthorRequestValidator : AbstractValidator<CreateAuthorRequest>
    {
        /// <summary>
        /// 
        /// </summary>
        public CreateAuthorRequestValidator()
        {
            RuleFor(p => p.Value).NotNull().NotEmpty().MaximumLength(150);
        }
    }
}
